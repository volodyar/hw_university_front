import './not-found.scss';

function NotFound() {
  return (
    <div className="container">
      <h1 className="not-found__title">NOT FOUND</h1>
      <p className="not-found__text">
        We are sorry, but the page you were looking for can’t be found...
      </p>
    </div>
  );
}

export default NotFound;
