import { CreateStudentForm } from 'components/view/components/students/create-student/create-student-form';

function CreateStudent() {
  return <CreateStudentForm />;
}

export default CreateStudent;
