import { useState } from 'react';
import { CreateCourseModal } from 'components/view/components/courses/create-course-modal/create-course-modal';
import { UpdateCourseModal } from 'components/view/components/courses/update-course-modal/update-course-modal';
import { useCreateModal } from './primary-shared-layout/primary-shared-layout';
import { CoursesList } from 'components/view/components/courses/courses-list/courses-list';
import { Course } from '../redux/courses';

function Courses() {
  const { isCreateModalOpen, setIsCreateModalOpen } = useCreateModal();
  const [isUpdateModalOpen, setIsUpdateModalOpen] = useState(false);
  const [course, setCourse] = useState<Course | null>(null);

  function handleUpdate(course: Course) {
    setCourse(course);
    setIsUpdateModalOpen(true);
  }

  return (
    <>
      <CoursesList handleUpdate={handleUpdate} />
      <CreateCourseModal
        isCreateModalOpen={isCreateModalOpen}
        setIsCreateModalOpen={setIsCreateModalOpen}
      />
      <UpdateCourseModal
        course={course}
        isUpdateModalOpen={isUpdateModalOpen}
        setIsUpdateModalOpen={setIsUpdateModalOpen}
      />
    </>
  );
}

export default Courses;
