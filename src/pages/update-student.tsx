import { UpdateStudentForm } from 'components/view/components/students/update-student/update-student-form';

function UpdateStudent() {
  return <UpdateStudentForm />;
}

export default UpdateStudent;
