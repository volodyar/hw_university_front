import { Outlet, useLocation, useOutletContext } from 'react-router-dom';
import { Header } from 'components/header/header';
import { SideBar } from 'components/sidebar/sidebar';
import { SearchBar } from 'components/view/components/search-bar/search-bar';
import { useState, Dispatch, SetStateAction, Suspense } from 'react';
import './primary-shared-layout.styles.scss';

function PrimarySharedLayout() {
  const [isCreateModalOpen, setIsCreateModalOpen] = useState(false);
  const { pathname } = useLocation();
  const title = pathname[1].toUpperCase() + pathname.substring(2);
  const headerTitle = <h1 className="header__title">{title}</h1>;

  return (
    <>
      <Header tag={headerTitle} />
      <main className="primary-page">
        <SideBar />
        <div className="primary-page__container">
          <SearchBar setIsCreateModalOpen={setIsCreateModalOpen} />
          <Suspense fallback={<div>Loading...</div>}>
            <Outlet context={{ isCreateModalOpen, setIsCreateModalOpen }} />
          </Suspense>
        </div>
      </main>
    </>
  );
}

type ContextType = {
  isCreateModalOpen: boolean;
  setIsCreateModalOpen: Dispatch<SetStateAction<boolean>>;
};

export function useCreateModal() {
  return useOutletContext<ContextType>();
}

export default PrimarySharedLayout;
