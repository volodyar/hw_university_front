import { Suspense } from 'react';
import { Outlet } from 'react-router-dom';

function Auth() {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Outlet />
    </Suspense>
  );
}

export default Auth;
