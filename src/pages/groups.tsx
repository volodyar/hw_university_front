import { useState } from 'react';
import { GroupsList } from 'components/view/components/groups/groups-list/groups-list';
import { UpdateGroupModal } from 'components/view/components/groups/update-group-modal/update-group-modal';
import { CreateGroupModal } from 'components/view/components/groups/create-group-modal/create-group-modal';
import { useCreateModal } from './primary-shared-layout/primary-shared-layout';
import { Group } from 'redux/groups';

function Groups() {
  const { isCreateModalOpen, setIsCreateModalOpen } = useCreateModal();
  const [isUpdateModalOpen, setIsUpdateModalOpen] = useState(false);
  const [group, setGroup] = useState<Group | null>(null);

  function handleUpdate(group: Group) {
    setGroup(group);
    setIsUpdateModalOpen(true);
  }

  return (
    <>
      <GroupsList handleUpdate={handleUpdate} />
      <CreateGroupModal
        isCreateModalOpen={isCreateModalOpen}
        setIsCreateModalOpen={setIsCreateModalOpen}
      />
      <UpdateGroupModal
        group={group}
        isUpdateModalOpen={isUpdateModalOpen}
        setIsUpdateModalOpen={setIsUpdateModalOpen}
      />
    </>
  );
}

export default Groups;
