import { ForgetPasswordForm } from 'components/view/components/forget-password-form/forget-password-form';

function ForgetPassword() {
  return <ForgetPasswordForm />;
}

export default ForgetPassword;
