import { Suspense } from 'react';
import { Outlet, Link } from 'react-router-dom';
import { Header } from 'components/header/header';
import { LogoSVG } from 'svg';
import './student-shared-layout.styles.scss';
import { Pathname } from 'utils/enums';

function StudentSharedLayout() {
  const logo = <LogoSVG className="header__logo"></LogoSVG>;

  return (
    <div className="student">
      <Header tag={logo} headerStyle="header_ml_0px" wrapperStyle="header__wrapper_p_15px_32px" />
      <div className="student__container">
        <Link className="back-button" to={Pathname.students}>
          <div className="back-button__text">Back</div>
        </Link>
        <Suspense fallback={<div>Loading...</div>}>
          <Outlet />
        </Suspense>
      </div>
    </div>
  );
}

export default StudentSharedLayout;
