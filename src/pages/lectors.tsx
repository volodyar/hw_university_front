import { useState } from 'react';
import { useCreateModal } from './primary-shared-layout/primary-shared-layout';
import { LectorsList } from 'components/view/components/lectors/lectors-list/lectors-list';
import { CreateLectorModal } from 'components/view/components/lectors/create-lector-modal/create-lector-modal';
import { UpdateLectorModal } from '../components/view/components/lectors/update-lector-modal/update-lector-modal';
import { Lector } from 'redux/lectors';

function Lectors() {
  const { isCreateModalOpen, setIsCreateModalOpen } = useCreateModal();
  const [isUpdateModalOpen, setIsUpdateModalOpen] = useState(false);
  const [lector, setLector] = useState<Lector | null>(null);

  function handleUpdate(lector: Lector) {
    setLector(lector);
    setIsUpdateModalOpen(true);
  }

  return (
    <>
      <LectorsList handleUpdate={handleUpdate} />
      <CreateLectorModal
        isCreateModalOpen={isCreateModalOpen}
        setIsCreateModalOpen={setIsCreateModalOpen}
      />
      <UpdateLectorModal
        lector={lector}
        isUpdateModalOpen={isUpdateModalOpen}
        setIsUpdateModalOpen={setIsUpdateModalOpen}
      />
    </>
  );
}

export default Lectors;
