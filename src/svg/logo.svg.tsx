type LogoProps = {
  className: string;
};

export function LogoSVG({ className }: LogoProps) {
  return (
    <svg
      className={className}
      width="79"
      height="80"
      viewBox="0 0 79 80"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M39.5 45.7333L19.7722 57.1556L0 45.7333L19.7722 34.3111L39.5 45.7333Z"
        fill="#D5B5FF"
      />
      <path d="M0 45.7333V68.5777L19.7722 80V57.1556L0 45.7333Z" fill="#A965FF" />
      <path d="M39.5 45.7333V68.5777L19.7722 80V57.1556L39.5 45.7333Z" fill="#7101FF" />
      <path
        d="M79 45.7333L59.2278 57.1556L39.5 45.7333L59.2278 34.3111L79 45.7333Z"
        fill="#D5B5FF"
      />
      <path d="M39.5 45.7333V68.5777L59.2278 80V57.1556L39.5 45.7333Z" fill="#A965FF" />
      <path d="M79 45.7333V68.5777L59.2278 80V57.1556L79 45.7333Z" fill="#7101FF" />
      <path
        d="M59.2278 11.4222L39.5 22.8444L19.7722 11.4222L39.5 0L59.2278 11.4222Z"
        fill="#D5B5FF"
      />
      <path d="M19.7722 11.4222V34.3111L39.5 45.7333V22.8444L19.7722 11.4222Z" fill="#A965FF" />
      <path d="M59.2278 11.4222V34.3111L39.5 45.7333V22.8444L59.2278 11.4222Z" fill="#7101FF" />
    </svg>
  );
}
