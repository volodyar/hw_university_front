export { CheckboxSVG } from './checkbox.svg';
export { LogoSVG } from './logo.svg';
export { ErrorSVG } from './error-svg';
export { LogoSecondSVG } from './logo-second.svg';

export { DashboardSVG } from './dashboard.svg';
export { CoursesSVG } from './courses.svg';
export { LectorsSVG } from './lectors.svg';
export { GroupSVG } from './group.svg';
export { StudentsSVG } from './students.svg';
export { LogoutSVG } from './logout.svg';

export { CrossSVG } from './cross.svg';
export { SearchLoopSVG } from './search-loop.svg';
export { EditEntitySVG } from './edit-enitity.svg';
export { CrossExitSVG } from './cross-exit.svg';
