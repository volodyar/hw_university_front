import userPhoto from 'images/user-photo.jpg';
import './header.styles.scss';

export function Header({
  tag,
  headerStyle,
  wrapperStyle,
}: {
  tag: React.ReactNode;
  headerStyle?: string;
  wrapperStyle?: string;
}) {
  return (
    <header className={`header ${headerStyle}`}>
      <div className={`header__wrapper ${wrapperStyle}`}>
        {tag}
        <img className="header__user-photo" src={userPhoto} alt="user" />
      </div>
    </header>
  );
}
