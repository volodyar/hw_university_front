import { useLocation, useNavigate } from 'react-router-dom';
import { useEffect, MouseEvent } from 'react';
import { useDispatch } from 'react-redux';
import { logout } from 'redux/auth/auth.slice';
import classNames from 'classnames';
import { useLogoutMutation } from 'redux/auth/auth-api.slice';
import { toast } from 'react-toastify';
import {
  CoursesSVG,
  DashboardSVG,
  GroupSVG,
  LectorsSVG,
  LogoSecondSVG,
  LogoutSVG,
  StudentsSVG,
} from 'svg';
import './sidebar.styles.scss';
import { Endpoints, Pathname } from 'utils/enums';

export function SideBar() {
  const [setLogout, { isError, isSuccess, isLoading }] = useLogoutMutation();
  const { pathname } = useLocation();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  async function onLogout() {
    await setLogout();
  }

  function onTab(e: MouseEvent<HTMLButtonElement>) {
    const { value } = e.currentTarget;
    if (value === Endpoints.groups) navigate(Pathname.groups);
    if (value === Endpoints.students) navigate(Pathname.students);
    if (value === Endpoints.lectors) navigate(Pathname.lectors);
    if (value === Endpoints.courses) navigate(Pathname.courses);
  }

  useEffect(() => {
    if (isSuccess) {
      dispatch(logout());
      const message = 'Log out successfully!';
      toast.success(message, { toastId: message });
      navigate('/login');
    }
  }, [isSuccess, navigate, dispatch]);

  useEffect(() => {
    if (isError) {
      const message = 'Something went wrong';
      toast.error(message, { toastId: message });
    }
  }, [isError]);

  return (
    <div className="sidebar">
      <div className="sidebar__wrapper">
        <div className="sidebar__logo-wrapper">
          <LogoSecondSVG className="sidebar__logo" />
        </div>
        <div className="sidebar__title-wrapper">
          <DashboardSVG className="sidebar__icon" />
          <h2 className="sidebar__title">Dashboard</h2>
        </div>
        <ul className="sidebar__list">
          <li
            className={classNames('sidebar__item', {
              sidebar__item_active: pathname === Pathname.courses,
            })}
          >
            <CoursesSVG className="sidebar__icon" />
            <button onClick={onTab} type="button" className="sidebar__button" value="courses">
              Courses
            </button>
          </li>
          <li
            className={classNames('sidebar__item', {
              sidebar__item_active: pathname === Pathname.lectors,
            })}
          >
            <LectorsSVG className="sidebar__icon" />
            <button onClick={onTab} type="button" className="sidebar__button" value="lectors">
              Lectors
            </button>
          </li>
          <li
            className={classNames('sidebar__item', {
              sidebar__item_active: pathname === Pathname.groups,
            })}
          >
            <GroupSVG className="sidebar__icon" />
            <button onClick={onTab} type="button" className="sidebar__button" value="groups">
              Groups
            </button>
          </li>
          <li
            className={classNames('sidebar__item', {
              sidebar__item_active: pathname === Pathname.students,
            })}
          >
            <StudentsSVG className="sidebar__icon" />
            <button onClick={onTab} type="button" className="sidebar__button" value="students">
              Students
            </button>
          </li>
        </ul>
      </div>
      <div className="sidebar__logout-wrapper">
        <LogoutSVG className="sidebar__icon" />
        <button type="button" className="sidebar__logout" onClick={onLogout}>
          {isLoading ? 'Loading...' : 'Log out'}
        </button>
      </div>
    </div>
  );
}
