import classNames from 'classnames';
import { CheckboxSVG } from 'svg';

interface CheckboxProps {
  onClick: React.MouseEventHandler<HTMLButtonElement>;
  isChecked: boolean;
}

export function Checkbox({ onClick, isChecked }: CheckboxProps) {
  return (
    <button
      type="button"
      aria-label="show password checkbox"
      onClick={onClick}
      className={classNames('checkbox', {
        checkbox_checked: isChecked,
      })}
    >
      {<CheckboxSVG className="checkbox__icon" />}
    </button>
  );
}
