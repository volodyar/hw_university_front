import { Navigate, Outlet, useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { selectIsLoggedIn } from 'redux/auth/auth.slice';

interface RestrictedRouteProps {
  redirectTo: string;
}

export function RestrictedRoute({ redirectTo }: RestrictedRouteProps) {
  const location = useLocation();
  const isLoggedIn = useSelector(selectIsLoggedIn);

  return isLoggedIn ? <Navigate to={redirectTo} state={{ from: location }} replace /> : <Outlet />;
}
