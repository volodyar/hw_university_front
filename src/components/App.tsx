import { lazy } from 'react';
import { Route, Routes } from 'react-router-dom';
import { RestrictedRoute } from './restricted-route';
import { PrivateRoute } from './private-route';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Auth from 'pages/auth';
import PrimarySharedLayout from 'pages/primary-shared-layout/primary-shared-layout';
import StudentSharedLayout from 'pages/student-shared-layout/student-shared-layout';
import NotFound from 'pages/not-found/not-found';
import { Endpoints, Pathname } from 'utils/enums';

const Groups = lazy(() => import('pages/groups'));
const Courses = lazy(() => import('pages/courses'));
const Lectors = lazy(() => import('pages/lectors'));
const Students = lazy(() => import('pages/students'));

const UpdateStudent = lazy(() => import('pages/update-student'));
const CreateStudent = lazy(() => import('pages/create-student'));

const Login = lazy(() => import('pages/login'));
const Register = lazy(() => import('pages/register'));
const ForgetPassword = lazy(() => import('pages/forget-password'));
const ResetPassword = lazy(() => import('pages/reset-password'));

function App() {
  return (
    <>
      <ToastContainer autoClose={1500} hideProgressBar />
      <Routes>
        {/* Restricted routes */}
        <Route element={<RestrictedRoute redirectTo={Pathname.lectors} />}>
          <Route path="/" element={<Auth />}>
            <Route index element={<Login />} />
            <Route path="login" element={<Login />} />
            <Route path="register" element={<Register />} />
            <Route path="reset-password" element={<ResetPassword />} />
            <Route path="forget-password" element={<ForgetPassword />} />
          </Route>
        </Route>
        {/* Private routes */}
        <Route element={<PrivateRoute redirectTo="/login" />}>
          <Route path="/" element={<PrimarySharedLayout />}>
            <Route path={Endpoints.groups} element={<Groups />} />
            <Route path={Endpoints.courses} element={<Courses />} />
            <Route path={Endpoints.students} element={<Students />} />
            <Route path={Endpoints.lectors} element={<Lectors />} />
          </Route>
          <Route path={Pathname.students} element={<StudentSharedLayout />}>
            <Route path="update/:id" element={<UpdateStudent />} />
            <Route path="create" element={<CreateStudent />} />
          </Route>
        </Route>
        <Route path="*" element={<NotFound />} />
      </Routes>
    </>
  );
}

export default App;
