import { ReactNode } from 'react';
import { LogoSVG } from 'svg';
import './auth-shared-layout.styles.scss';

type AuthSharedLayoutProp = {
  title: string;
  text?: string | undefined;
  children: ReactNode;
};

function AuthSharedLayout({ title, children, text }: AuthSharedLayoutProp) {
  return (
    <div className="auth-page">
      <div className="auth-page__container">
        <LogoSVG className="auth-page__logo" />
        <h1 className="auth-page__caption">{title}</h1>
        <p className="auth-page__text">{text}</p>
        {children}
      </div>
    </div>
  );
}

export default AuthSharedLayout;
