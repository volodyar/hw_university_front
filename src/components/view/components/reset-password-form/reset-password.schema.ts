import { object, string, ref } from 'yup';

export const resetPasswordFormSchema = object({
  password: string().min(6, 'Too Short!').max(18, 'Too Long!').required('Required'),
  confirmPassword: string()
    .oneOf([ref('password')], 'Passwords must match')
    .required('Required'),
});
