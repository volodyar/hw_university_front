import { useEffect, useState } from 'react';
import { Link, useSearchParams } from 'react-router-dom';
import { ErrorMessage, Field, Formik } from 'formik';
import classNames from 'classnames';
import { InferType } from 'yup';
import { Checkbox } from 'components/common/checkbox';
import { ErrorSVG } from 'svg';
import { toast } from 'react-toastify';
import { resetPasswordFormSchema } from './reset-password.schema';
import AuthSharedLayout from 'components/layouts/auth-shared-layout/auth-shared-layout';
import { useResetPasswordMutation } from 'redux/auth/auth-api.slice';
import { resetPasswordExceptionFilter } from 'utils/exception-filter';

interface IResetPasswordProps extends InferType<typeof resetPasswordFormSchema> {}
export interface IResetPasswordRequest {
  password: string;
  token: string;
}

export function ResetPasswordForm() {
  const [showPassword, setShowPassword] = useState(false);
  const [resetPassword, { isSuccess, isError, isLoading, error }] = useResetPasswordMutation();

  const [searchParam] = useSearchParams();
  const token = searchParam.get('token') as string;

  useEffect(() => {
    if (isError) {
      const message = resetPasswordExceptionFilter(error);
      toast.error(message, { toastId: message, autoClose: 3000 });
    }
  }, [isError, error]);

  if (isSuccess) {
    const message = 'Password successfully updated';
    toast.success(message, { toastId: message, autoClose: 3000 });

    return (
      <AuthSharedLayout title="Password Changed">
        <p className="auth-page__text auth-page__text_center">
          You can use your new password to log into your account
        </p>
        <Link to="/login" className="button button_size_large button_interaction">
          Log in
        </Link>
        <Link to="/" className="button button_borderless button_interaction">
          Go to Home
        </Link>
      </AuthSharedLayout>
    );
  }

  return (
    <AuthSharedLayout title="Reset Your Password">
      <Formik
        initialValues={{
          password: '',
          confirmPassword: '',
        }}
        validationSchema={resetPasswordFormSchema}
        onSubmit={(values: IResetPasswordProps, { resetForm }) => {
          resetPassword({ password: values.password, token });
          resetForm();
        }}
      >
        {({ handleChange, handleSubmit, values, errors, touched }) => {
          const isPasswordError = touched.password && errors.password;
          const isConfirmPasswordError = touched.confirmPassword && errors.confirmPassword;
          return (
            <form className="form" onSubmit={handleSubmit}>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isPasswordError,
                  })}
                  htmlFor="password"
                >
                  Password
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isPasswordError,
                  })}
                  name="password"
                  id="password"
                  type={showPassword ? 'text' : 'password'}
                  onChange={handleChange}
                  value={values.password}
                  autoComplete="new-password"
                  placeholder="password"
                  disabled={isLoading}
                />
                <ErrorMessage name="password">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isPasswordError,
                  })}
                />
              </div>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isConfirmPasswordError,
                  })}
                  htmlFor="confirmPassword"
                >
                  Confirm Password
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isConfirmPasswordError,
                  })}
                  name="confirmPassword"
                  id="confirmPassword"
                  type={showPassword ? 'text' : 'password'}
                  onChange={handleChange}
                  value={values.confirmPassword}
                  autoComplete="new-password"
                  placeholder="confirm password"
                  disabled={isLoading}
                />
                <ErrorMessage name="confirmPassword">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isConfirmPasswordError,
                  })}
                />
              </div>
              <label className="form__show-password">
                <Checkbox
                  onClick={() => setShowPassword((value) => !value)}
                  isChecked={showPassword}
                />
                <span className="form__label form__label_m_0px">Show Password</span>
              </label>
              <button
                type="submit"
                className="button button_size_large button_interaction"
                disabled={isLoading}
              >
                Reset
              </button>
            </form>
          );
        }}
      </Formik>
    </AuthSharedLayout>
  );
}
