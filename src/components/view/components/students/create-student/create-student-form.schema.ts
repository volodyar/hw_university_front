import { mixed, number, object, string } from 'yup';
import { FILE_EXTENSIONS, MAX_FILE_SIZE } from 'utils/constants';

export const createStudentSchema = object({
  name: string().required('Required'),
  surname: string().required('Required'),
  email: string().required('Required'),
  age: string().required('Required'),
  group: object().shape({
    label: string().required(),
    value: number().required(),
  }),
  course: object().shape({
    label: string().required(),
    value: number().required(),
  }),
  files: mixed()
    .required('Photo is required')
    .test('is-valid-type', 'Not a valid image type', (value) => {
      const filesList = value as FileList;
      return filesList[0] && FILE_EXTENSIONS.indexOf(filesList[0].type) > -1;
    })
    .test('is-valid-size', 'Max allowed size is 100KB', (value) => {
      const fileList = value as FileList;
      return fileList[0] && fileList[0].size <= MAX_FILE_SIZE;
    }),
});
