import { useEffect } from 'react';
import { ErrorMessage, Field, Formik, FormikHelpers } from 'formik';
import Select from 'react-select';
import { useDispatch } from 'react-redux';
import { CrossSVG, ErrorSVG } from 'svg';
import { InferType } from 'yup';
import { toast } from 'react-toastify';
import classNames from 'classnames';
import { createStudentSchema } from './create-student-form.schema';
import { modalExceptionFilter } from 'utils/exception-filter';
import { Student, useCreateStudentMutation } from 'redux/students';
import { useFindAllGroupsQuery } from 'redux/groups';
import { useFindAllCoursesQuery } from 'redux/courses';

export interface IStudentAdd extends InferType<typeof createStudentSchema> {}

export function CreateStudentForm() {
  const { data: groupsData } = useFindAllGroupsQuery({
    search: '?sort=name&order=ASC',
    limit: 100,
    offset: 1,
  });
  const { data: courseData } = useFindAllCoursesQuery({
    search: '?sort=name&order=ASC',
    limit: 100,
    offset: 1,
  });
  const [createStudent, { data, error, isError, isSuccess, isLoading }] =
    useCreateStudentMutation();
  const dispatch = useDispatch();

  async function onSubmit(values: IStudentAdd, { resetForm }: FormikHelpers<IStudentAdd>) {
    const { group, course, files } = values;
    const filesList = files as FileList;
    const photo = filesList[0] as File;

    const { data } = (await createStudent({ ...values, group, course, photo })) as {
      data: Student;
    };

    if (data?.id) {
      resetForm();
    }
  }

  useEffect(() => {
    if (isSuccess && data?.id) {
      const message = 'Created successfully!';
      toast.success(message, {
        toastId: message,
      });
    }
  }, [isSuccess, data, dispatch]);

  useEffect(() => {
    if (isError) {
      const message = modalExceptionFilter(error);
      toast.error(message, { toastId: message });
    }
  }, [isError, error]);

  const groupsOption = groupsData?.groups.map((group) => ({ value: group.id, label: group.name }));
  const coursesOption = courseData?.courses.map((course) => ({
    value: course.id,
    label: course.name,
  }));

  const initialValue: IStudentAdd = {
    name: '',
    surname: '',
    email: '',
    age: '',
    group: { label: 'Group', value: 0 },
    course: { label: 'Course', value: 0 },
    files: '',
  };

  return (
    <>
      <Formik
        initialValues={initialValue}
        validationSchema={createStudentSchema}
        onSubmit={onSubmit}
      >
        {({
          handleChange,
          handleSubmit,
          values,
          touched,
          errors,
          setFieldValue,
          setFieldTouched,
        }) => {
          const isFormTouched = Object.values(touched).length > 0;
          const isNameError = touched.name && errors.name;
          const isSurnameError = touched.surname && errors.surname;
          const isEmailError = touched.email && errors.email;
          const isAgeError = touched.age && errors.age;
          const isFilesError = touched.files && errors.files;
          const filesError = errors.files as string;

          const fileList = values.files as FileList;
          const file = fileList[0];

          return (
            <form className="form form_student" onSubmit={handleSubmit}>
              <div>
                <h1 className="form__caption form__caption_align_left">Personal information</h1>
                <div className="form__photo-wrapper">
                  <div className="form__input-wrapper form__input-wrapper_m_0px">
                    {file !== undefined ? (
                      <img
                        className="form__photo"
                        width="168"
                        height="168"
                        src={URL.createObjectURL(file)}
                        alt="student"
                      />
                    ) : (
                      <label htmlFor="photo" className="form__upload-outter-circle">
                        <input
                          type="file"
                          name="photo"
                          id="photo"
                          className="form__upload-input"
                          onChange={({ currentTarget }) =>
                            setFieldValue('files', currentTarget.files)
                          }
                        />
                        <div className="form__upload-inner-circle">
                          <CrossSVG className="form__upload-svg" />
                        </div>
                      </label>
                    )}
                    {isFilesError ? <div className="form__error-text">{filesError}</div> : null}
                  </div>
                  <div className="form__replace-photo-wrapper">
                    <label className="form__replace-input-wrapper">
                      <input
                        type="file"
                        name="replace-photo"
                        id="replace"
                        className="form__replace-input"
                        disabled={file === undefined}
                        onChange={({ currentTarget }) =>
                          setFieldValue('files', currentTarget.files)
                        }
                      />
                      <div
                        className={classNames('form__replace-button', {
                          'form__replace-button_disabled': file === undefined,
                        })}
                      >
                        Replace
                      </div>
                    </label>
                    {file === undefined ? (
                      <p className="form__photo-info form__photo-info_m_0px">No file chosen</p>
                    ) : null}
                    <p className="form__photo-info">
                      Must be a .jpg or .png file smaller than 10MB and at least 400px by 400px.
                    </p>
                  </div>
                </div>
                <div className="form__input-wrapper">
                  <label
                    className={classNames('form__label', {
                      form__label_error: isNameError,
                    })}
                    htmlFor="name"
                  >
                    Name
                  </label>
                  <Field
                    className={classNames('form__input', {
                      form__input_error: isNameError,
                    })}
                    name="name"
                    type="text"
                    id="name"
                    disabled={isLoading}
                    onChange={handleChange}
                    value={values.name}
                    autoComplete="off"
                    placeholder="John"
                  />
                  <ErrorMessage name="name">
                    {(msg: string) => <div className="form__error-text">{msg}</div>}
                  </ErrorMessage>
                  <ErrorSVG
                    className={classNames('form__error-icon', {
                      'form__error-icon_error': isNameError,
                    })}
                  />
                </div>
                <div className="form__input-wrapper">
                  <label
                    className={classNames('form__label', {
                      form__label_error: isSurnameError,
                    })}
                    htmlFor="surname"
                  >
                    Surname
                  </label>
                  <Field
                    className={classNames('form__input', {
                      form__input_error: isSurnameError,
                    })}
                    name="surname"
                    type="text"
                    id="surname"
                    disabled={isLoading}
                    onChange={handleChange}
                    value={values.surname}
                    autoComplete="off"
                    placeholder="Smith"
                  />
                  <ErrorMessage name="surname">
                    {(msg: string) => <div className="form__error-text">{msg}</div>}
                  </ErrorMessage>
                  <ErrorSVG
                    className={classNames('form__error-icon', {
                      'form__error-icon_error': isSurnameError,
                    })}
                  />
                </div>
                <div className="form__input-wrapper">
                  <label
                    className={classNames('form__label', {
                      form__label_error: isEmailError,
                    })}
                    htmlFor="email"
                  >
                    Email
                  </label>
                  <Field
                    className={classNames('form__input', {
                      form__input_error: isEmailError,
                    })}
                    name="email"
                    type="email"
                    id="email"
                    disabled={isLoading}
                    onChange={handleChange}
                    value={values.email}
                    autoComplete="off"
                    placeholder="name@mail.com"
                  />
                  <ErrorMessage name="email">
                    {(msg: string) => <div className="form__error-text">{msg}</div>}
                  </ErrorMessage>
                  <ErrorSVG
                    className={classNames('form__error-icon', {
                      'form__error-icon_error': isEmailError,
                    })}
                  />
                </div>
                <div className="form__input-wrapper">
                  <label
                    className={classNames('form__label', {
                      form__label_error: isAgeError,
                    })}
                    htmlFor="age"
                  >
                    Age
                  </label>
                  <Field
                    className={classNames('form__input', {
                      form__input_error: isAgeError,
                    })}
                    name="age"
                    id="age"
                    disabled={isLoading}
                    type="text"
                    onChange={handleChange}
                    value={values.age}
                    placeholder="Age"
                  />
                  <ErrorSVG
                    className={classNames('form__error-icon', {
                      'form__error-icon_error': isAgeError,
                    })}
                  />
                  <ErrorMessage name="age">
                    {(msg: string) => <div className="form__error-text">{msg}</div>}
                  </ErrorMessage>
                </div>
                <button
                  type="submit"
                  className={classNames('button button_m_0px button_size_large', {
                    button_interaction: isFormTouched,
                    button_disabled: !isFormTouched,
                  })}
                  disabled={isLoading || !isFormTouched}
                >
                  {isLoading ? 'Loading...' : 'Save changes'}
                </button>
              </div>
              <div>
                <h2 className="form__caption form__caption_align_left">Courses and Groups</h2>
                <div className="form__input-wrapper">
                  <label className="form__label" htmlFor="groups">
                    Groups
                  </label>
                  <Select
                    id="groups"
                    classNamePrefix="student-select"
                    className="student-select"
                    value={values.group}
                    onChange={(option) => {
                      setFieldValue('group', option);
                      setFieldTouched('group');
                    }}
                    options={groupsOption}
                  />
                </div>
                <div className="form__input-wrapper">
                  <label className="form__label" htmlFor="courses">
                    Course
                  </label>
                  <Select
                    id="courses"
                    classNamePrefix="student-select"
                    className="student-select"
                    value={values.course}
                    onChange={(option) => {
                      setFieldValue('course', option);
                      setFieldTouched('course');
                    }}
                    options={coursesOption}
                  />
                </div>
              </div>
            </form>
          );
        }}
      </Formik>
    </>
  );
}
