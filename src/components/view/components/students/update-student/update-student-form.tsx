import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { ErrorMessage, Field, Formik, FormikHelpers } from 'formik';
import Select from 'react-select';
import { CrossSVG, ErrorSVG } from 'svg';
import { InferType } from 'yup';
import { toast } from 'react-toastify';
import classNames from 'classnames';
import { updateStudentSchema } from './update-student-form.schema';
import { modalExceptionFilter } from 'utils/exception-filter';
import { Student, useLazyFindOneStudentQuery, useUpdateStudentMutation } from 'redux/students';
import { useFindAllGroupsQuery } from 'redux/groups';
import { useFindAllCoursesQuery } from 'redux/courses';

import { SERVER_HOST } from 'utils/constants';

const uploadPhotoInput = document.getElementById('photo') as HTMLInputElement;
const replacePhotoInput = document.getElementById('replace') as HTMLInputElement;
export interface IStudentUpdate extends InferType<typeof updateStudentSchema> {
  imagePath: string;
}

export function UpdateStudentForm() {
  const { id } = useParams();
  const { data: groupsData } = useFindAllGroupsQuery({
    search: '?sort=name&order=ASC',
    limit: 100,
    offset: 1,
  });
  const { data: courseData } = useFindAllCoursesQuery({
    search: '?sort=name&order=ASC',
    limit: 100,
    offset: 1,
  });
  const [updateStudent, { data, error, isError, isSuccess, isLoading }] =
    useUpdateStudentMutation();
  const [findOneStudent, { data: student, isSuccess: isStudentRetrived }] =
    useLazyFindOneStudentQuery();

  async function onSubmit(values: IStudentUpdate, { resetForm }: FormikHelpers<IStudentUpdate>) {
    const { group, course, files } = values;
    let photo;

    if (files) {
      const filesList = files as FileList;
      photo = filesList[0] as File;
    }

    if (id) {
      const { data } = (await updateStudent({
        body: { ...values, group, course, photo },
        id,
      })) as {
        data: Student;
      };

      if (data?.id) {
        uploadPhotoInput.value = '';
        replacePhotoInput.value = '';
        resetForm();
      }
    }
  }

  useEffect(() => {
    if (id !== undefined) {
      findOneStudent({ id });
    }
  }, [findOneStudent, id]);

  useEffect(() => {
    if (isSuccess && data?.id) {
      const message = 'Created successfully!';
      toast.success(message, {
        toastId: message,
      });
    }
  }, [isSuccess, data]);

  useEffect(() => {
    if (isError) {
      const message = modalExceptionFilter(error);
      toast.error(message, { toastId: message });
    }
  }, [isError, error]);

  const groupsOption = groupsData?.groups.map((group) => ({ value: group.id, label: group.name }));
  const coursesOption = courseData?.courses.map((course) => ({
    value: course.id,
    label: course.name,
  }));

  const initialValue: IStudentUpdate = {
    name: student?.name!,
    surname: student?.surname!,
    email: student?.email!,
    age: student?.age.toString()!,
    group: { label: student?.group?.name!, value: student?.groupId! },
    course: { label: 'Course', value: 0 },
    imagePath: `${SERVER_HOST}/public/${student?.imagePath!}`,
    files: '',
  };

  return (
    <>
      {isStudentRetrived && (
        <Formik
          initialValues={initialValue}
          validationSchema={updateStudentSchema}
          onSubmit={onSubmit}
        >
          {({
            handleChange,
            handleSubmit,
            values,
            touched,
            errors,
            setFieldValue,
            setFieldTouched,
          }) => {
            const isFormTouched = Object.values(touched).length > 0;
            const isNameError = touched.name && errors.name;
            const isSurnameError = touched.surname && errors.surname;
            const isEmailError = touched.email && errors.email;
            const isAgeError = touched.age && errors.age;
            const isFilesError = touched.files && errors.files;
            const filesError = errors.files as string;

            const fileList = values.files as FileList;
            const file = fileList[0];

            return (
              <form className="form form_student" onSubmit={handleSubmit}>
                <div>
                  <h1 className="form__caption form__caption_align_left">Personal information</h1>
                  <div className="form__photo-wrapper">
                    <div className="form__input-wrapper form__input-wrapper_m_0px">
                      {file !== undefined || student?.imagePath !== null ? (
                        <img
                          className="form__photo"
                          width="168"
                          height="168"
                          src={file === undefined ? values.imagePath : URL.createObjectURL(file)}
                          alt="student"
                        />
                      ) : (
                        <label htmlFor="photo" className="form__upload-outter-circle">
                          <input
                            type="file"
                            name="photo"
                            id="photo"
                            className="form__upload-input"
                            onChange={({ currentTarget }) =>
                              setFieldValue('files', currentTarget.files)
                            }
                          />
                          <div className="form__upload-inner-circle">
                            <CrossSVG className="form__upload-svg" />
                          </div>
                        </label>
                      )}
                      {isFilesError ? <div className="form__error-text">{filesError}</div> : null}
                    </div>
                    <div className="form__replace-photo-wrapper">
                      <label className="form__replace-input-wrapper">
                        <input
                          type="file"
                          name="replace-photo"
                          id="replace"
                          className="form__replace-input"
                          onChange={({ currentTarget }) => {
                            setFieldValue('files', currentTarget.files);
                            setFieldTouched('files');
                          }}
                        />
                        <div className="form__replace-button">Replace</div>
                      </label>
                      {isFilesError ? <div className="form__error-text">{filesError}</div> : null}
                      <p className="form__photo-info">
                        Must be a .jpg or .png file smaller than 10MB and at least 400px by 400px.
                      </p>
                    </div>
                  </div>
                  <div className="form__input-wrapper">
                    <label
                      className={classNames('form__label', {
                        form__label_error: isNameError,
                      })}
                      htmlFor="name"
                    >
                      Name
                    </label>
                    <Field
                      className={classNames('form__input', {
                        form__input_error: isNameError,
                      })}
                      name="name"
                      type="text"
                      id="name"
                      disabled={isLoading}
                      onChange={handleChange}
                      value={values.name}
                      autoComplete="off"
                      placeholder="John"
                    />
                    <ErrorMessage name="name">
                      {(msg: string) => <div className="form__error-text">{msg}</div>}
                    </ErrorMessage>
                    <ErrorSVG
                      className={classNames('form__error-icon', {
                        'form__error-icon_error': isNameError,
                      })}
                    />
                  </div>
                  <div className="form__input-wrapper">
                    <label
                      className={classNames('form__label', {
                        form__label_error: isSurnameError,
                      })}
                      htmlFor="surname"
                    >
                      Surname
                    </label>
                    <Field
                      className={classNames('form__input', {
                        form__input_error: isSurnameError,
                      })}
                      name="surname"
                      type="text"
                      id="surname"
                      disabled={isLoading}
                      onChange={handleChange}
                      value={values.surname}
                      autoComplete="off"
                      placeholder="Smith"
                    />
                    <ErrorMessage name="surname">
                      {(msg: string) => <div className="form__error-text">{msg}</div>}
                    </ErrorMessage>
                    <ErrorSVG
                      className={classNames('form__error-icon', {
                        'form__error-icon_error': isSurnameError,
                      })}
                    />
                  </div>
                  <div className="form__input-wrapper">
                    <label
                      className={classNames('form__label', {
                        form__label_error: isEmailError,
                      })}
                      htmlFor="email"
                    >
                      Email
                    </label>
                    <Field
                      className={classNames('form__input', {
                        form__input_error: isEmailError,
                      })}
                      name="email"
                      type="email"
                      id="email"
                      disabled={isLoading}
                      onChange={handleChange}
                      value={values.email}
                      autoComplete="off"
                      placeholder="name@mail.com"
                    />
                    <ErrorMessage name="email">
                      {(msg: string) => <div className="form__error-text">{msg}</div>}
                    </ErrorMessage>
                    <ErrorSVG
                      className={classNames('form__error-icon', {
                        'form__error-icon_error': isEmailError,
                      })}
                    />
                  </div>
                  <div className="form__input-wrapper">
                    <label
                      className={classNames('form__label', {
                        form__label_error: isAgeError,
                      })}
                      htmlFor="age"
                    >
                      Age
                    </label>
                    <Field
                      className={classNames('form__input', {
                        form__input_error: isAgeError,
                      })}
                      name="age"
                      id="age"
                      disabled={isLoading}
                      type="text"
                      onChange={handleChange}
                      value={values.age}
                      placeholder="Age"
                    />
                    <ErrorSVG
                      className={classNames('form__error-icon', {
                        'form__error-icon_error': isAgeError,
                      })}
                    />
                    <ErrorMessage name="age">
                      {(msg: string) => <div className="form__error-text">{msg}</div>}
                    </ErrorMessage>
                  </div>
                  <button
                    type="submit"
                    className={classNames('button button_size_large', {
                      button_interaction: isFormTouched,
                      button_disabled: !isFormTouched,
                    })}
                    disabled={isLoading || !isFormTouched}
                  >
                    {isLoading ? 'Loading...' : 'Save changes'}
                  </button>
                </div>
                <div>
                  <h2 className="form__caption form__caption_align_left">Courses and Groups</h2>
                  <div className="form__input-wrapper">
                    <label className="form__label" htmlFor="groups">
                      Groups
                    </label>
                    <Select
                      id="groups"
                      classNamePrefix="student-select"
                      className="student-select"
                      value={values.group}
                      onChange={(option) => {
                        setFieldValue('group', option);
                        setFieldTouched('group');
                      }}
                      options={groupsOption}
                    />
                  </div>
                  <div className="form__input-wrapper">
                    <label className="form__label" htmlFor="courses">
                      Course
                    </label>
                    <Select
                      id="courses"
                      classNamePrefix="student-select"
                      className="student-select"
                      value={values.course}
                      onChange={(option) => {
                        setFieldValue('course', option);
                        setFieldTouched('course');
                      }}
                      options={coursesOption}
                    />
                  </div>
                </div>
              </form>
            );
          }}
        </Formik>
      )}
    </>
  );
}
