import { mixed, number, object, string } from 'yup';
import { FILE_EXTENSIONS, MAX_FILE_SIZE } from 'utils/constants';

export const updateStudentSchema = object({
  name: string().required('Required'),
  surname: string().required('Required'),
  email: string().required('Required'),
  age: string().required('Required'),
  group: object().shape({
    label: string().required(),
    value: number().required(),
  }),
  course: object().shape({
    label: string().required(),
    value: number().required(),
  }),
  files: mixed()
    .optional()
    .test('is-valid-type', 'Not a valid image type', (value) => {
      if (value instanceof FileList) {
        const filesList = value as FileList;
        return filesList[0] && FILE_EXTENSIONS.indexOf(filesList[0].type) > -1;
      } else {
        return true;
      }
    })
    .test('is-valid-size', 'Max allowed size is 100KB', (value) => {
      if (value instanceof FileList) {
				const fileList = value as FileList;

        return fileList[0] && fileList[0].size <= MAX_FILE_SIZE;
      } else {
        return true;
      }
    }),
});
