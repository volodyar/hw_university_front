import { useState } from 'react';
import { Student } from 'redux/students';
import Select from 'react-select';
import { EditEntitySVG } from 'svg';
import studentPhoto from 'images/student_photo.png';
import { Link, useLocation } from 'react-router-dom';
import { SERVER_HOST } from 'utils/constants';
import './students-item.styles.scss';
import { Pathname } from 'utils/enums';

type StudnetsItemProps = {
  student: Student;
};

export function StudentsItem({ student }: StudnetsItemProps) {
  const location = useLocation();
  const courseOptions = student.courses.map((course) => ({
    value: course.id,
    label: course.name,
  }));
  const [defaultOption, setDefaultOption] = useState(courseOptions[0]);

  return (
    <li className="entities-list__item entities-list__item_students">
      <img
        className="entities-list__photo"
        src={student.imagePath ? `${SERVER_HOST}/public/${student.imagePath}` : studentPhoto}
        alt="students"
      />
      <div className="entities-list__text">{student.name}</div>
      <div className="entities-list__text">{student.surname}</div>
      <div className="entities-list__text">{student.email}</div>
      <div className="entities-list__text">{student.age}</div>
      <Select
        classNamePrefix="students-course-select"
        className="students-course-select"
        value={defaultOption}
        options={courseOptions}
        onChange={(option) => setDefaultOption(option!)}
      />
      <div className="entities-list__text">{student.group?.name ?? 'None'}</div>
      <Link
        to={`${Pathname.students}/update/${student.id}`}
        className="entities-list__button button_interaction"
        aria-label="edit entity"
        state={{ from: location }}
      >
        <EditEntitySVG className="entities-list__svg" />
      </Link>
    </li>
  );
}
