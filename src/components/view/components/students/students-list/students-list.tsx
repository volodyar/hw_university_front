import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import Pagination from 'rc-pagination';
import {
  selectStudents,
  selectStudentsCount,
  setStudents,
  useLazyFindAllStudentsQuery,
} from 'redux/students';
import { PER_PAGE } from 'utils/constants';
import { StudentsItem } from '../students-item/students-item';

const listTitles = ['Name', 'Surname', 'Email', 'Age', 'Course', 'Group'];

export function StudentsList() {
  const [currentPage, setCurrentPage] = useState(1);
  const { search } = useLocation();
  const [findAllStudents, { data, isSuccess }] = useLazyFindAllStudentsQuery();

  const dispatch = useDispatch();
  const students = useSelector(selectStudents);
  const count = useSelector(selectStudentsCount);

  useEffect(() => {
    setCurrentPage(1);
  }, [search]);

  useEffect(() => {
    if (search !== '') {
      findAllStudents({ search, limit: PER_PAGE, offset: currentPage });
    }
  }, [search, currentPage, findAllStudents]);

  useEffect(() => {
    if (isSuccess && data) {
      dispatch(setStudents({ students: data.students, count: data.count }));
    }
  }, [isSuccess, data, dispatch]);

  const handlePageClick = (page: number) => {
    setCurrentPage(page);
  };

  return (
    <>
      <ul className="title-list title-list_students">
        {listTitles.map((title, index) => (
          <li key={index} className="title-list__item">
            {title}
          </li>
        ))}
      </ul>
      <ul className="entities-list">
        {students.map((student) => (
          <StudentsItem key={student.id} student={student}></StudentsItem>
        ))}
      </ul>
      <Pagination
        total={count}
        pageSize={PER_PAGE}
        current={currentPage}
        onChange={handlePageClick}
        hideOnSinglePage={true}
        prevIcon={'<'}
        nextIcon={'>'}
      />
    </>
  );
}
