import { emailPattern } from 'utils/constants';
import { object, string, ref } from 'yup';

export const registerFormSchema = object({
  email: string()
    .email('Invalid email')
    .matches(emailPattern, { message: 'Invalid email' })
    .required('Required'),
  password: string().min(6, 'Too Short!').max(18, 'Too Long!').required('Required'),
  confirmPassword: string()
    .oneOf([ref('password')], 'Passwords must match')
    .required('Required'),
});
