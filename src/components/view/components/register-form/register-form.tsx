import { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { ErrorMessage, Field, Formik } from 'formik';
import { InferType } from 'yup';
import classNames from 'classnames';
import { Checkbox } from 'components/common/checkbox';
import { ErrorSVG } from 'svg';
import { toast } from 'react-toastify';
import { registerFormSchema } from './register.schema';
import { useRegisterMutation } from 'redux/auth/auth-api.slice';
import { signupExceptionFilter } from 'utils/exception-filter';
import AuthSharedLayout from 'components/layouts/auth-shared-layout/auth-shared-layout';
import 'react-toastify/dist/ReactToastify.css';

export interface IRegisterRequest extends InferType<typeof registerFormSchema> {}

export function RegisterForm() {
  const [showPassword, setShowPassword] = useState(false);
  const [setSignup, { error, isError, isSuccess, isLoading }] = useRegisterMutation();
  const navigate = useNavigate();

  async function onSubmit(values: IRegisterRequest) {
    await setSignup(values);
  }

  useEffect(() => {
    if (isSuccess) {
      const message = 'Successfully registered!';
      toast.success(message, {
        toastId: message,
      });
      navigate('/login');
    }
  }, [isSuccess, navigate]);

  useEffect(() => {
    if (isError) {
      const message = signupExceptionFilter(error);
      toast.error(message, { toastId: message });
    }
  }, [isError, error]);

  return (
    <AuthSharedLayout title="Register your account">
      <Formik
        initialValues={{
          email: '',
          password: '',
          confirmPassword: '',
        }}
        validationSchema={registerFormSchema}
        onSubmit={onSubmit}
      >
        {({ handleChange, handleSubmit, values, touched, errors }) => {
          const isEmailError = touched.email && errors.email;
          const isPasswordError = touched.password && errors.password;
          const isConfirmPasswordError = touched.confirmPassword && errors.confirmPassword;

          return (
            <form className="form" onSubmit={handleSubmit}>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isEmailError,
                  })}
                  htmlFor="email"
                >
                  Email
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isEmailError,
                  })}
                  name="email"
                  type="email"
                  id="email"
                  disabled={isLoading}
                  onChange={handleChange}
                  value={values.email}
                  autoComplete="off"
                  placeholder="name@mail.com"
                />
                <ErrorMessage name="email">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isEmailError,
                  })}
                />
              </div>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isPasswordError,
                  })}
                  htmlFor="password"
                >
                  Password
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isPasswordError,
                  })}
                  name="password"
                  id="password"
                  disabled={isLoading}
                  type={showPassword ? 'text' : 'password'}
                  onChange={handleChange}
                  value={values.password}
                  autoComplete="new-password"
                  placeholder="password"
                />
                <ErrorMessage name="password">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isPasswordError,
                  })}
                />
              </div>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isConfirmPasswordError,
                  })}
                  htmlFor="confirmPassword"
                >
                  Confirm Password
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isConfirmPasswordError,
                  })}
                  name="confirmPassword"
                  id="confirmPassword"
                  type={showPassword ? 'text' : 'password'}
                  onChange={handleChange}
                  disabled={isLoading}
                  value={values.confirmPassword}
                  autoComplete="new-password"
                  placeholder="confirm password"
                />
                <ErrorMessage name="confirmPassword">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isConfirmPasswordError,
                  })}
                />
              </div>
              <label className="form__show-password">
                <Checkbox
                  onClick={() => setShowPassword((value) => !value)}
                  isChecked={showPassword}
                />
                <span className="form__label form__label_m_0px">Show Password</span>
              </label>
              <button
                type="submit"
                className="button button_size_large button_interaction"
                disabled={isLoading}
              >
                {isLoading ? 'Loading...' : 'Register'}
              </button>
              <Link to="/login" className="button button_borderless button_interaction">
                Login
              </Link>
            </form>
          );
        }}
      </Formik>
    </AuthSharedLayout>
  );
}
