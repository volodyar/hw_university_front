import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { ErrorMessage, Field, Formik } from 'formik';
import { InferType } from 'yup';
import classNames from 'classnames';
import { toast } from 'react-toastify';
import { Checkbox } from 'components/common/checkbox';
import { ErrorSVG } from 'svg';
import { loginFormSchema } from './login.schema';
import { setCredentials } from 'redux/auth/auth.slice';
import { useLoginMutation } from 'redux/auth/auth-api.slice';
import { loginExceptionFilter } from 'utils/exception-filter';
import AuthSharedLayout from 'components/layouts/auth-shared-layout/auth-shared-layout';

export interface ILoginRequest extends InferType<typeof loginFormSchema> {}

export function LoginForm() {
  const dispatch = useDispatch();
  const [showPassword, setShowPassword] = useState(false);
  const [login, { error, data, isError, isSuccess, isLoading }] = useLoginMutation();

  async function onSubmit(values: ILoginRequest) {
    await login(values);
  }

  useEffect(() => {
    if (isSuccess && data) {
      dispatch(setCredentials(data));
      const message = 'Log in successfully!';
      toast.success(message, {
        toastId: message,
      });
    }
  }, [isSuccess, data, dispatch]);

  useEffect(() => {
    if (isError) {
      const message = loginExceptionFilter(error);
      toast.error(message, { toastId: message });
    }
  }, [isError, error]);

  return (
    <AuthSharedLayout title="Welcome!">
      <Formik
        initialValues={{
          email: '',
          password: '',
        }}
        validationSchema={loginFormSchema}
        onSubmit={onSubmit}
      >
        {({ handleChange, handleSubmit, values, errors, touched }) => {
          const isEmailError = touched.email && errors.email;
          const isPasswordError = touched.password && errors.password;

          return (
            <form className="form" onSubmit={handleSubmit}>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isEmailError,
                  })}
                  htmlFor="email"
                >
                  Email
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isEmailError,
                  })}
                  name="email"
                  type="email"
                  id="email"
                  disabled={isLoading}
                  onChange={handleChange}
                  value={values.email}
                  autoComplete="off"
                  placeholder="name@mail.com"
                />
                <ErrorMessage name="email">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isEmailError,
                  })}
                />
              </div>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isPasswordError,
                  })}
                  htmlFor="password"
                >
                  Password
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isPasswordError,
                  })}
                  name="password"
                  id="password"
                  disabled={isLoading}
                  type={showPassword ? 'text' : 'password'}
                  onChange={handleChange}
                  value={values.password}
                  autoComplete="new-password"
                  placeholder="password"
                />
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isPasswordError,
                  })}
                />
                <ErrorMessage name="password">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
              </div>
              <div className="form__input-wrapper form__input-wrapper_flex_row">
                <label className="form__show-password form__show-password_mb_0px">
                  <Checkbox
                    onClick={() => setShowPassword((value) => !value)}
                    isChecked={showPassword}
                  />
                  <span className="form__label form__label_m_0px">Show Password</span>
                </label>
                <Link to="/forget-password" className="form__label form__label_m_0px">
                  Reset Password
                </Link>
              </div>
              <button
                type="submit"
                className="button button_size_large button_interaction"
                disabled={isLoading}
              >
                {isLoading ? 'Loading...' : 'Login'}
              </button>
              <Link to="/register" className="button button_borderless button_interaction">
                Register
              </Link>
            </form>
          );
        }}
      </Formik>
    </AuthSharedLayout>
  );
}
