import { emailPattern } from 'utils/constants';
import { object, string } from 'yup';

export const loginFormSchema = object({
  email: string()
    .email('Invalid email')
    .matches(emailPattern, { message: 'Invalid email' })
    .required('Required'),
  password: string().min(6, 'Too Short!').max(16, 'Too Long!').required('Required'),
});
