import { Link } from 'react-router-dom';
import { ErrorMessage, Field, Formik } from 'formik';
import { useEffect } from 'react';
import { InferType } from 'yup';
import { ErrorSVG } from 'svg';
import classNames from 'classnames';
import { toast } from 'react-toastify';
import { forgetPasswordFormSchema } from './forget-password.schema';
import AuthSharedLayout from 'components/layouts/auth-shared-layout/auth-shared-layout';
import { useResetPasswordRequestMutation } from 'redux/auth/auth-api.slice';
import { forgetPasswordExceptionFilter } from 'utils/exception-filter';

export interface IForgetPasswordRequest extends InferType<typeof forgetPasswordFormSchema> {}

export function ForgetPasswordForm() {
  const [resetPassword, { isSuccess, isError, isLoading, error }] =
    useResetPasswordRequestMutation();

  useEffect(() => {
    if (isSuccess) {
      const message = 'Request password is accepted. Please check your email!';
      toast.success(message, {
        toastId: message,
      });
    }
  }, [isSuccess]);

  useEffect(() => {
    if (isError) {
      const message = forgetPasswordExceptionFilter(error);
      toast.error(message, { toastId: message, autoClose: 3000 });
    }
  }, [isError, error]);

  const text =
    "Don't worry, happens to the best of us. Enter the email address associated with your account and we'll send you a link to reset.";
  return (
    <AuthSharedLayout title="Reset Password" text={text}>
      <Formik
        initialValues={{
          email: '',
        }}
        validationSchema={forgetPasswordFormSchema}
        onSubmit={(values: IForgetPasswordRequest, { resetForm }) => {
          resetPassword(values);
          resetForm();
        }}
      >
        {({ handleChange, handleSubmit, values, errors }) => (
          <form className="form" onSubmit={handleSubmit}>
            <div className="form__input-wrapper">
              <label
                className={classNames('form__label', {
                  form__label_error: errors.email,
                })}
                htmlFor="email"
              >
                Email
              </label>
              <Field
                className={classNames('form__input', {
                  form__input_error: errors.email,
                })}
                name="email"
                type="email"
                id="email"
                onChange={handleChange}
                value={values.email}
                autoComplete="off"
                placeholder="name@mail.com"
                disabled={isLoading}
              />
              <ErrorMessage name="email">
                {(msg: string) => <div className="form__error-text">{msg}</div>}
              </ErrorMessage>
              <ErrorSVG
                className={classNames('form__error-icon', {
                  'form__error-icon_error': errors.email,
                })}
              />
            </div>
            <button
              type="submit"
              className="button button_size_large button_interaction"
              disabled={isLoading}
            >
              Reset
            </button>
            <Link to="/login" className="button button_borderless button_interaction">
              Cancel
            </Link>
          </form>
        )}
      </Formik>
    </AuthSharedLayout>
  );
}
