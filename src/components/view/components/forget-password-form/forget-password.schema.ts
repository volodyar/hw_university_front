import { emailPattern } from 'utils/constants';
import { object, string } from 'yup';

export const forgetPasswordFormSchema = object({
  email: string()
    .email('Invalid email')
    .matches(emailPattern, { message: 'Invalid email' })
    .required('Required'),
});
