import { EditEntitySVG } from 'svg';
import { Group } from 'redux/groups';

type GroupItemProps = {
  group: Group;
  handleUpdate: (group: Group) => void;
};

export function GroupsItem({ group, handleUpdate }: GroupItemProps) {
  return (
    <li className="entities-list__item entities-list__item_groups">
      <div className="entities-list__text">{group.name}</div>
      <button
        onClick={() => handleUpdate(group)}
        type="button"
        className="entities-list__button button_interaction"
        aria-label="edit entity"
      >
        <EditEntitySVG className="entities-list__svg" />
      </button>
    </li>
  );
}
