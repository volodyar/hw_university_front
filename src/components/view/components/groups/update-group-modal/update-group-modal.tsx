import { Dispatch, SetStateAction, useEffect } from 'react';
import { ErrorMessage, Field, Formik, FormikHelpers } from 'formik';
import { useDispatch } from 'react-redux';
import Modal from 'react-modal';
import { InferType } from 'yup';
import ReactModal from 'react-modal';
import classNames from 'classnames';
import { toast } from 'react-toastify';
import { modalExceptionFilter } from 'utils/exception-filter';
import { CrossExitSVG, ErrorSVG } from 'svg';
import { updateGroupSchema } from './update-group.schema';
import { Group, useUpdateGroupMutation } from 'redux/groups';

ReactModal.setAppElement('#modal');
export interface IGroupUpdate extends InferType<typeof updateGroupSchema> {}

type GroupModalProps = {
  isUpdateModalOpen: boolean;
  setIsUpdateModalOpen: Dispatch<SetStateAction<boolean>>;
  group: Group | null;
};

export function UpdateGroupModal({
  isUpdateModalOpen,
  setIsUpdateModalOpen,
  group,
}: GroupModalProps) {
  const [updateGroup, { data, error, isError, isSuccess, isLoading }] = useUpdateGroupMutation();
  const dispatch = useDispatch();

  async function onSubmit(values: IGroupUpdate, { resetForm }: FormikHelpers<IGroupUpdate>) {
    const res = (await updateGroup({ body: values, id: group?.id! })) as { data: Group };

    if (res?.data?.id) {
      resetForm();
    }
  }

  useEffect(() => {
    if (isSuccess && data?.id) {
      const message = 'Updated successfully!';
      toast.success(message, {
        toastId: message,
      });

      setIsUpdateModalOpen(false);
    }
  }, [isSuccess, data, dispatch, setIsUpdateModalOpen]);

  useEffect(() => {
    if (isError) {
      const message = modalExceptionFilter(error);
      toast.error(message, { toastId: message });
    }
  }, [isError, error]);

  return (
    <Modal isOpen={isUpdateModalOpen} className="modal" overlayClassName="overlay">
      <Formik
        initialValues={{
          name: group?.name!,
        }}
        validationSchema={updateGroupSchema}
        onSubmit={onSubmit}
      >
        {({ handleChange, handleSubmit, values, touched, errors }) => {
          const isNameError = touched.name && errors.name;

          return (
            <form className="form" onSubmit={handleSubmit}>
              <h1 className="form__caption">Update group</h1>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isNameError,
                  })}
                  htmlFor="name"
                >
                  Name
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isNameError,
                  })}
                  name="name"
                  type="text"
                  id="name"
                  disabled={isLoading}
                  onChange={handleChange}
                  value={values.name}
                  autoComplete="off"
                  placeholder="MS-40"
                />
                <ErrorMessage name="name">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isNameError,
                  })}
                />
              </div>
              <button
                type="submit"
                className="button button_size_large button_interaction"
                disabled={isLoading}
              >
                {isLoading ? 'Loading...' : 'Save changes'}
              </button>
            </form>
          );
        }}
      </Formik>
      <button
        type="button"
        className="modal__close-button button_interaction"
        onClick={() => setIsUpdateModalOpen(false)}
      >
        <CrossExitSVG className="modal__close-svg" />
      </button>
    </Modal>
  );
}
