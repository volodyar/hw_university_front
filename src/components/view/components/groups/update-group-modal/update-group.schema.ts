import { object, string } from 'yup';

export const updateGroupSchema = object({
  name: string().required('Required'),
});
