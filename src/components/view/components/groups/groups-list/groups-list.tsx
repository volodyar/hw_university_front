import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Pagination from 'rc-pagination';
import {
  setGroups,
  selectGroups,
  selectGroupsCount,
  useLazyFindAllGroupsQuery,
  Group,
} from 'redux/groups';
import { GroupsItem } from '../groups-item/groups-item';
import { PER_PAGE } from 'utils/constants';

import { useLocation } from 'react-router-dom';

interface IGroupsListProps {
  handleUpdate: (group: Group) => void;
}

const listTitles = ['Name'];

export function GroupsList({ handleUpdate }: IGroupsListProps) {
  const [currentPage, setCurrentPage] = useState(1);
  const { search } = useLocation();
  const [findAllGroups, { data, isSuccess }] = useLazyFindAllGroupsQuery();

  const dispatch = useDispatch();
  const groups = useSelector(selectGroups);
  const count = useSelector(selectGroupsCount);

  useEffect(() => {
    setCurrentPage(1);
  }, [search]);

  useEffect(() => {
    if (search !== '') {
      findAllGroups({ search, limit: PER_PAGE, offset: currentPage });
    }
  }, [search, currentPage, findAllGroups]);

  useEffect(() => {
    if (isSuccess && data) {
      dispatch(setGroups({ groups: data.groups, count: data.count }));
    }
  }, [isSuccess, data, dispatch]);

  const handlePageClick = (page: number) => {
    setCurrentPage(page);
  };

  return (
    <>
      <ul className="title-list title-list_groups">
        {listTitles.map((title, index) => (
          <li key={index} className="title-list__item">
            {title}
          </li>
        ))}
      </ul>
      <ul className="entities-list">
        {groups.map((group) => (
          <GroupsItem key={group.id} handleUpdate={handleUpdate} group={group} />
        ))}
      </ul>
      <Pagination
        total={count}
        pageSize={PER_PAGE}
        current={currentPage}
        onChange={handlePageClick}
        hideOnSinglePage={true}
        prevIcon={'<'}
        nextIcon={'>'}
      />
    </>
  );
}
