import { object, string } from 'yup';

export const createGroupSchema = object({
  name: string().required('Required'),
});
