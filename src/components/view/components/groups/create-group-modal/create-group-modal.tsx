import { Dispatch, SetStateAction, useEffect } from 'react';
import { ErrorMessage, Field, Formik, FormikHelpers } from 'formik';
import { useDispatch } from 'react-redux';
import Modal from 'react-modal';
import ReactModal from 'react-modal';
import { InferType } from 'yup';
import classNames from 'classnames';
import { toast } from 'react-toastify';
import { Group, useCreateGroupMutation } from 'redux/groups';
import { modalExceptionFilter } from 'utils/exception-filter';
import { CrossExitSVG, ErrorSVG } from 'svg';
import { createGroupSchema } from './create-groups.schema';

ReactModal.setAppElement('#modal');
export interface IGroupsAdd extends InferType<typeof createGroupSchema> {}

type GroupModalProps = {
  isCreateModalOpen: boolean;
  setIsCreateModalOpen: Dispatch<SetStateAction<boolean>>;
};

export function CreateGroupModal({ isCreateModalOpen, setIsCreateModalOpen }: GroupModalProps) {
  const [createGroup, { data, error, isError, isSuccess, isLoading }] = useCreateGroupMutation();
  const dispatch = useDispatch();

  async function onSubmit(values: IGroupsAdd, { resetForm }: FormikHelpers<IGroupsAdd>) {
    const res = (await createGroup(values)) as { data: Group };
    if (res?.data?.id) {
      resetForm();
    }
  }

  useEffect(() => {
    if (isSuccess && data?.id) {
      const message = 'Created successfully!';
      toast.success(message, {
        toastId: message,
      });
    }
  }, [isSuccess, data, dispatch]);

  useEffect(() => {
    if (isError) {
      const message = modalExceptionFilter(error);
      toast.error(message, { toastId: message });
    }
  }, [isError, error]);

  return (
    <Modal isOpen={isCreateModalOpen} className="modal" overlayClassName="overlay">
      <Formik
        initialValues={{
          name: '',
        }}
        validationSchema={createGroupSchema}
        onSubmit={onSubmit}
      >
        {({ handleChange, handleSubmit, values, touched, errors }) => {
          const isNameError = touched.name && errors.name;

          return (
            <form className="form" onSubmit={handleSubmit}>
              <h1 className="form__caption">Add new group</h1>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isNameError,
                  })}
                  htmlFor="name"
                >
                  Name
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isNameError,
                  })}
                  name="name"
                  type="text"
                  id="name"
                  disabled={isLoading}
                  onChange={handleChange}
                  value={values.name}
                  autoComplete="off"
                  placeholder="MS-40"
                />
                <ErrorMessage name="name">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isNameError,
                  })}
                />
              </div>
              <button
                type="submit"
                className="button button_size_large button_interaction"
                disabled={isLoading}
              >
                {isLoading ? 'Loading...' : 'Create'}
              </button>
            </form>
          );
        }}
      </Formik>
      <button
        type="button"
        className="modal__close-button button_interaction"
        onClick={() => setIsCreateModalOpen(false)}
      >
        <CrossExitSVG className="modal__close-svg" />
      </button>
    </Modal>
  );
}
