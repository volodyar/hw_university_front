import { Dispatch, SetStateAction, useEffect } from 'react';
import { ErrorMessage, Field, Formik, FormikHelpers } from 'formik';
import { useDispatch } from 'react-redux';
import Modal from 'react-modal';
import ReactModal from 'react-modal';
import { InferType } from 'yup';
import classNames from 'classnames';
import { toast } from 'react-toastify';
import { modalExceptionFilter } from 'utils/exception-filter';
import { CrossExitSVG, ErrorSVG } from 'svg';
import { createCourseSchema } from './create-course.schema';
import { Course, useCreateCourseMutation } from 'redux/courses';

ReactModal.setAppElement('#modal');
export interface ICourseAdd extends InferType<typeof createCourseSchema> {}

type CourseModalProps = {
  isCreateModalOpen: boolean;
  setIsCreateModalOpen: Dispatch<SetStateAction<boolean>>;
};

export function CreateCourseModal({ isCreateModalOpen, setIsCreateModalOpen }: CourseModalProps) {
  const [createCourse, { data, error, isError, isSuccess, isLoading }] = useCreateCourseMutation();
  const dispatch = useDispatch();

  async function onSubmit(values: ICourseAdd, { resetForm }: FormikHelpers<ICourseAdd>) {
    const res = (await createCourse(values)) as { data: Course };
    if (res?.data?.id) {
      resetForm();
    }
  }

  useEffect(() => {
    if (isSuccess && data?.id) {
      const message = 'Created successfully!';
      toast.success(message, {
        toastId: message,
      });
    }
  }, [isSuccess, data, dispatch]);

  useEffect(() => {
    if (isError) {
      const message = modalExceptionFilter(error);
      toast.error(message, { toastId: message });
    }
  }, [isError, error]);

  return (
    <Modal isOpen={isCreateModalOpen} className="modal" overlayClassName="overlay">
      <Formik
        initialValues={{
          name: '',
          description: '',
          hours: '',
        }}
        validationSchema={createCourseSchema}
        onSubmit={onSubmit}
      >
        {({ handleChange, handleSubmit, values, touched, errors }) => {
          const isNameError = touched.name && errors.name;
          const isDescriptionError = touched.description && errors.description;
          const isHoursError = touched.hours && errors.hours;

          return (
            <form className="form" onSubmit={handleSubmit}>
              <h1 className="form__caption">Add new course</h1>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isNameError,
                  })}
                  htmlFor="name"
                >
                  Name
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isNameError,
                  })}
                  name="name"
                  type="text"
                  id="name"
                  disabled={isLoading}
                  onChange={handleChange}
                  value={values.name}
                  autoComplete="off"
                  placeholder="Math"
                />
                <ErrorMessage name="name">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isNameError,
                  })}
                />
              </div>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isDescriptionError,
                  })}
                  htmlFor="description"
                >
                  Description
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isDescriptionError,
                  })}
                  name="description"
                  type="text"
                  id="description"
                  disabled={isLoading}
                  onChange={handleChange}
                  value={values.description}
                  autoComplete="off"
                  placeholder="A lot of useful information"
                />
                <ErrorMessage name="description">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isDescriptionError,
                  })}
                />
              </div>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isHoursError,
                  })}
                  htmlFor="hours"
                >
                  Hours
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isHoursError,
                  })}
                  name="hours"
                  type="text"
                  id="hours"
                  disabled={isLoading}
                  onChange={handleChange}
                  value={values.hours}
                  autoComplete="off"
                  placeholder="Hours"
                />
                <ErrorMessage name="hours">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isHoursError,
                  })}
                />
              </div>
              <button
                type="submit"
                className="button button_size_large button_interaction"
                disabled={isLoading}
              >
                {isLoading ? 'Loading...' : 'Create'}
              </button>
            </form>
          );
        }}
      </Formik>
      <button
        type="button"
        className="modal__close-button button_interaction"
        onClick={() => setIsCreateModalOpen(false)}
      >
        <CrossExitSVG className="modal__close-svg" />
      </button>
    </Modal>
  );
}
