import { object, string } from 'yup';

export const createCourseSchema = object({
  name: string().required('Required'),
  description: string().required('Required'),
  hours: string().required('Required'),
});
