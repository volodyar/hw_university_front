import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  setCourses,
  selectCourses,
  selectCoursesCount,
  useLazyFindAllCoursesQuery,
  Course,
} from 'redux/courses';
import Pagination from 'rc-pagination';
import { CoursesItem } from '../courses-item/courses-item';
import { PER_PAGE } from 'utils/constants';
import { useLocation } from 'react-router-dom';

interface ICoursesListProps {
  handleUpdate: (course: Course) => void;
}

const listTitles = ['Name', 'Description', 'Hours', 'Students count'];

export function CoursesList({ handleUpdate }: ICoursesListProps) {
  const [currentPage, setCurrentPage] = useState(1);
  const { search } = useLocation();
  const [findAllCourses, { data, isSuccess }] = useLazyFindAllCoursesQuery();

  const dispatch = useDispatch();
  const courses = useSelector(selectCourses);
  const count = useSelector(selectCoursesCount);

  useEffect(() => {
    setCurrentPage(1);
  }, [search]);

  useEffect(() => {
    if (search !== '') {
      findAllCourses({ search, limit: PER_PAGE, offset: currentPage });
    }
  }, [search, currentPage, findAllCourses]);

  useEffect(() => {
    if (isSuccess && data) {
      dispatch(setCourses({ courses: data.courses, count: data.count }));
    }
  }, [isSuccess, data, dispatch]);

  const handlePageClick = (page: number) => {
    setCurrentPage(page);
  };

  return (
    <>
      <ul className="title-list title-list_courses">
        {listTitles.map((title, index) => (
          <li key={index} className="title-list__item">
            {title}
          </li>
        ))}
      </ul>
      <ul className="entities-list">
        {courses.map((course) => (
          <CoursesItem key={course.id} handleUpdate={handleUpdate} course={course} />
        ))}
      </ul>
      <Pagination
        total={count}
        pageSize={PER_PAGE}
        current={currentPage}
        onChange={handlePageClick}
        hideOnSinglePage={true}
        prevIcon={'<'}
        nextIcon={'>'}
      />
    </>
  );
}
