import { object, string } from 'yup';

export const updateCourseSchema = object().shape(
  {
    name: string().when(['description', 'hours'], {
      is: (description: string, hours: number) => !description && !hours,
      then: (schema) => schema.required('At least one field must be fullfilled'),
    }),
    description: string().when(['name', 'hours'], {
      is: (name: string, hours: number) => !name && !hours,
      then: (schema) => schema.required('At least one field must be fullfilled'),
    }),
    hours: string().when(['name', 'description'], {
      is: (name: string, description: string) => !name && !description,
      then: (schema) => schema.required('At least one field must be fullfilled'),
    }),
  },
  [
    ['name', 'hours'],
    ['name', 'description'],
    ['hours', 'name'],
    ['hours', 'description'],
    ['description', 'name'],
    ['description', 'hours'],
  ],
);


