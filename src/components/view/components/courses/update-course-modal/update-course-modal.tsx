import { Dispatch, SetStateAction, useEffect } from 'react';
import { ErrorMessage, Field, Formik, FormikHelpers } from 'formik';
import { useDispatch } from 'react-redux';
import Modal from 'react-modal';
import { InferType } from 'yup';
import ReactModal from 'react-modal';
import classNames from 'classnames';
import { toast } from 'react-toastify';
import { modalExceptionFilter } from 'utils/exception-filter';
import { CrossExitSVG, ErrorSVG } from 'svg';
import { updateCourseSchema } from './update-course.schema';
import { Course, useUpdateCourseMutation } from 'redux/courses';

ReactModal.setAppElement('#modal');
export interface ICourseUpdate extends InferType<typeof updateCourseSchema> {}

type CourseModalProps = {
  isUpdateModalOpen: boolean;
  setIsUpdateModalOpen: Dispatch<SetStateAction<boolean>>;
  course: Course | null;
};

export function UpdateCourseModal({
  isUpdateModalOpen,
  setIsUpdateModalOpen,
  course,
}: CourseModalProps) {
  const [updateCourse, { data, error, isError, isSuccess, isLoading }] = useUpdateCourseMutation();
  const dispatch = useDispatch();

  async function onSubmit(values: ICourseUpdate, { resetForm }: FormikHelpers<ICourseUpdate>) {
    const object: ICourseUpdate = {};
    if (values.name !== '') object.name = values.name;
    if (values.description !== '') object.description = values.description;
    if (values.hours !== '') object.hours = values.hours;

    const res = (await updateCourse({ body: object, id: course?.id! })) as { data: Course };
    if (res?.data?.id) {
      resetForm();
    }
  }

  useEffect(() => {
    if (isSuccess && data?.id) {
      const message = 'Updated successfully!';
      toast.success(message, {
        toastId: message,
      });

      setIsUpdateModalOpen(false);
    }
  }, [isSuccess, data, dispatch, setIsUpdateModalOpen]);

  useEffect(() => {
    if (isError) {
      const message = modalExceptionFilter(error);
      toast.error(message, { toastId: message });
    }
  }, [isError, error]);

  const initialValues: ICourseUpdate = {
    name: course?.name!,
    description: course?.description!,
    hours: course?.hours!.toString(),
  };

  return (
    <Modal isOpen={isUpdateModalOpen} className="modal" overlayClassName="overlay">
      <Formik
        initialValues={initialValues}
        validationSchema={updateCourseSchema}
        onSubmit={onSubmit}
      >
        {({ handleChange, handleSubmit, values, touched, errors }) => {
          const isNameError = touched.name && errors.name;
          const isDescriptionError = touched.description && errors.description;
          const isHoursError = touched.hours && errors.hours;

          return (
            <form className="form" onSubmit={handleSubmit}>
              <h1 className="form__caption">Update the course</h1>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isNameError,
                  })}
                  htmlFor="name"
                >
                  Name
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isNameError,
                  })}
                  name="name"
                  type="text"
                  id="name"
                  disabled={isLoading}
                  onChange={handleChange}
                  value={values.name}
                  autoComplete="off"
                  placeholder="Math"
                />
                <ErrorMessage name="name">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isNameError,
                  })}
                />
              </div>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isDescriptionError,
                  })}
                  htmlFor="description"
                >
                  Description
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isDescriptionError,
                  })}
                  name="description"
                  type="text"
                  id="description"
                  disabled={isLoading}
                  onChange={handleChange}
                  value={values.description}
                  autoComplete="off"
                  placeholder="A lot of useful information"
                />
                <ErrorMessage name="description">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isDescriptionError,
                  })}
                />
              </div>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isHoursError,
                  })}
                  htmlFor="hours"
                >
                  Hours
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isHoursError,
                  })}
                  name="hours"
                  type="text"
                  id="hours"
                  disabled={isLoading}
                  onChange={handleChange}
                  value={values.hours}
                  autoComplete="off"
                  placeholder="46"
                />
                <ErrorMessage name="hours">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isHoursError,
                  })}
                />
              </div>
              <button
                type="submit"
                className="button button_size_large button_interaction"
                disabled={isLoading}
              >
                {isLoading ? 'Loading...' : 'Save changes'}
              </button>
            </form>
          );
        }}
      </Formik>
      <button
        type="button"
        className="modal__close-button button_interaction"
        onClick={() => setIsUpdateModalOpen(false)}
      >
        <CrossExitSVG className="modal__close-svg" />
      </button>
    </Modal>
  );
}
