import { EditEntitySVG } from 'svg';
import { Course } from 'redux/courses';

type CourseItemProps = {
  course: Course;
  handleUpdate: (course: Course) => void;
};

export function CoursesItem({ course, handleUpdate }: CourseItemProps) {
  return (
    <li className="entities-list__item entities-list__item_courses">
      <div className="entities-list__text">{course.name}</div>
      <div className="entities-list__text">{course.description}</div>
      <div className="entities-list__text">{course.hours}</div>
      <div className="entities-list__text">{course.studentsCount}</div>
      <button
        onClick={() => handleUpdate(course)}
        type="button"
        className="entities-list__button button_interaction"
        aria-label="edit entity"
      >
        <EditEntitySVG className="entities-list__svg" />
      </button>
    </li>
  );
}
