import { object, string } from 'yup';

export const createLectorSchema = object({
  name: string().required('Required'),
  surname: string().required('Required'),
  email: string().required('Required'),
  password: string().required('Required'),
});
