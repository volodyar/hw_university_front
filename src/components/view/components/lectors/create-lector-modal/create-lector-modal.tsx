import { Dispatch, SetStateAction, useEffect } from 'react';
import { ErrorMessage, Field, Formik, FormikHelpers } from 'formik';
import { useDispatch } from 'react-redux';
import Modal from 'react-modal';
import ReactModal from 'react-modal';
import { InferType } from 'yup';
import classNames from 'classnames';
import { toast } from 'react-toastify';
import { modalExceptionFilter } from 'utils/exception-filter';
import { CrossExitSVG, ErrorSVG } from 'svg';
import { createLectorSchema } from './create-lectors.schema';
import { Lector, useCreateLectorMutation } from 'redux/lectors';

ReactModal.setAppElement('#modal');
export interface ILectorAdd extends InferType<typeof createLectorSchema> {}

type LectorModalProps = {
  isCreateModalOpen: boolean;
  setIsCreateModalOpen: Dispatch<SetStateAction<boolean>>;
};

export function CreateLectorModal({ isCreateModalOpen, setIsCreateModalOpen }: LectorModalProps) {
  const [createLector, { data, error, isError, isSuccess, isLoading }] = useCreateLectorMutation();
  const dispatch = useDispatch();

  async function onSubmit(values: ILectorAdd, { resetForm }: FormikHelpers<ILectorAdd>) {
    const res = (await createLector(values)) as { data: Lector };
    if (res?.data?.id) {
      resetForm();
    }
  }

  useEffect(() => {
    if (isSuccess && data?.id) {
      const message = 'Created successfully!';
      toast.success(message, {
        toastId: message,
      });
    }
  }, [isSuccess, data, dispatch]);

  useEffect(() => {
    if (isError) {
      const message = modalExceptionFilter(error);
      toast.error(message, { toastId: message });
    }
  }, [isError, error]);

  return (
    <Modal isOpen={isCreateModalOpen} className="modal" overlayClassName="overlay">
      <Formik
        initialValues={{
          name: '',
          surname: '',
          email: '',
          password: '',
        }}
        validationSchema={createLectorSchema}
        onSubmit={onSubmit}
      >
        {({ handleChange, handleSubmit, values, touched, errors }) => {
          const isNameError = touched.name && errors.name;
          const isSurnameError = touched.surname && errors.surname;
          const isEmailError = touched.email && errors.email;
          const isPasswordError = touched.password && errors.password;

          return (
            <form className="form" onSubmit={handleSubmit}>
              <h1 className="form__caption">Add new lector</h1>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isNameError,
                  })}
                  htmlFor="name"
                >
                  Name
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isNameError,
                  })}
                  name="name"
                  type="text"
                  id="name"
                  disabled={isLoading}
                  onChange={handleChange}
                  value={values.name}
                  autoComplete="off"
                  placeholder="John"
                />
                <ErrorMessage name="name">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isNameError,
                  })}
                />
              </div>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isSurnameError,
                  })}
                  htmlFor="surname"
                >
                  Surname
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isSurnameError,
                  })}
                  name="surname"
                  type="text"
                  id="surname"
                  disabled={isLoading}
                  onChange={handleChange}
                  value={values.surname}
                  autoComplete="off"
                  placeholder="Smith"
                />
                <ErrorMessage name="surname">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isSurnameError,
                  })}
                />
              </div>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isEmailError,
                  })}
                  htmlFor="email"
                >
                  Email
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isEmailError,
                  })}
                  name="email"
                  type="email"
                  id="email"
                  disabled={isLoading}
                  onChange={handleChange}
                  value={values.email}
                  autoComplete="off"
                  placeholder="name@mail.com"
                />
                <ErrorMessage name="email">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isEmailError,
                  })}
                />
              </div>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isPasswordError,
                  })}
                  htmlFor="password"
                >
                  Password
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isPasswordError,
                  })}
                  name="password"
                  id="password"
                  disabled={isLoading}
                  type="password"
                  onChange={handleChange}
                  value={values.password}
                  autoComplete="new-password"
                  placeholder="password"
                />
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isPasswordError,
                  })}
                />
                <ErrorMessage name="password">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
              </div>
              <button
                type="submit"
                className="button button_size_large button_interaction"
                disabled={isLoading}
              >
                {isLoading ? 'Loading...' : 'Create'}
              </button>
            </form>
          );
        }}
      </Formik>
      <button
        type="button"
        className="modal__close-button button_interaction"
        onClick={() => setIsCreateModalOpen(false)}
      >
        <CrossExitSVG className="modal__close-svg" />
      </button>
    </Modal>
  );
}
