import { EditEntitySVG } from 'svg';
import { Lector } from 'redux/lectors';

type LectorItemProps = {
  lector: Lector;
  handleUpdate: (lector: Lector) => void;
};

export function LectorsItem({ lector, handleUpdate }: LectorItemProps) {
  return (
    <li className="entities-list__item">
      <div className="entities-list__text">{lector.name ?? 'None'}</div>
      <div className="entities-list__text">{lector.surname ?? 'None'}</div>
      <div className="entities-list__text">{lector.email}</div>
      <div className="entities-list__text">{lector.password}</div>
      <button
        type="button"
        onClick={() => handleUpdate(lector)}
        className="entities-list__button button_interaction"
        aria-label="edit entity"
      >
        <EditEntitySVG className="entities-list__svg" />
      </button>
    </li>
  );
}
