import { object, string } from 'yup';

export const updateLectorSchema = object().shape(
  {
    name: string().when(['surname', 'email'], {
      is: (surname: string, email: number) => !surname && !email,
      then: (schema) => schema.required('At least one field must be fullfilled'),
    }),
    surname: string().when(['name', 'email'], {
      is: (name: string, email: number) => !name && !email,
      then: (schema) => schema.required('At least one field must be fullfilled'),
    }),
    email: string().when(['name', 'surname'], {
      is: (name: string, surname: string) => !name && !surname,
      then: (schema) => schema.required('At least one field must be fullfilled'),
    }),
  },
  [
    ['name', 'email'],
    ['name', 'surname'],
    ['email', 'name'],
    ['email', 'surname'],
    ['surname', 'name'],
    ['surname', 'email'],
  ],
);
