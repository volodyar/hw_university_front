import { Dispatch, SetStateAction, useEffect } from 'react';
import { ErrorMessage, Field, Formik, FormikHelpers } from 'formik';
import { useDispatch } from 'react-redux';
import Modal from 'react-modal';
import { InferType } from 'yup';
import ReactModal from 'react-modal';
import classNames from 'classnames';
import { toast } from 'react-toastify';
import { modalExceptionFilter } from 'utils/exception-filter';
import { CrossExitSVG, ErrorSVG } from 'svg';
import { updateLectorSchema } from './update-lector.schema';
import { Lector, useUpdateLectorMutation } from 'redux/lectors';

ReactModal.setAppElement('#modal');
export interface ILectorUpdate extends InferType<typeof updateLectorSchema> {}

type LectorModalProps = {
  isUpdateModalOpen: boolean;
  setIsUpdateModalOpen: Dispatch<SetStateAction<boolean>>;
  lector: Lector | null;
};

export function UpdateLectorModal({
  isUpdateModalOpen,
  setIsUpdateModalOpen,
  lector,
}: LectorModalProps) {
  const [updateLector, { data, error, isError, isSuccess, isLoading }] = useUpdateLectorMutation();
  const dispatch = useDispatch();

  async function onSubmit(values: ILectorUpdate, { resetForm }: FormikHelpers<ILectorUpdate>) {
    const object: ILectorUpdate = {};
    if (values.name !== '') object.name = values.name;
    if (values.surname !== '') object.surname = values.surname;
    if (values.email !== '') object.email = values.email;

    const res = (await updateLector({ body: object, id: lector?.id! })) as { data: Lector };
    if (res?.data?.id) {
      resetForm();
    }
  }

  useEffect(() => {
    if (isSuccess && data?.id) {
      const message = 'Updated successfully!';
      toast.success(message, {
        toastId: message,
      });

      setIsUpdateModalOpen(false);
    }
  }, [isSuccess, data, dispatch, setIsUpdateModalOpen]);

  useEffect(() => {
    if (isError) {
      const message = modalExceptionFilter(error);
      toast.error(message, { toastId: message });
    }
  }, [isError, error]);

  const initialValues: ILectorUpdate = {
    name: lector?.name!,
    surname: lector?.surname!,
    email: lector?.email!,
  };

  return (
    <Modal isOpen={isUpdateModalOpen} className="modal" overlayClassName="overlay">
      <Formik
        initialValues={initialValues}
        validationSchema={updateLectorSchema}
        onSubmit={onSubmit}
      >
        {({ handleChange, handleSubmit, values, touched, errors }) => {
          const isNameError = touched.name && errors.name;
          const isSurnameError = touched.surname && errors.surname;
          const isEmailError = touched.email && errors.email;

          return (
            <form className="form" onSubmit={handleSubmit}>
              <h1 className="form__caption">Update lector</h1>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isNameError,
                  })}
                  htmlFor="name"
                >
                  Name
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isNameError,
                  })}
                  name="name"
                  type="text"
                  id="name"
                  disabled={isLoading}
                  onChange={handleChange}
                  value={values.name}
                  autoComplete="off"
                  placeholder="John"
                />
                <ErrorMessage name="name">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isNameError,
                  })}
                />
              </div>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isSurnameError,
                  })}
                  htmlFor="surname"
                >
                  Surname
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isSurnameError,
                  })}
                  name="surname"
                  type="text"
                  id="surname"
                  disabled={isLoading}
                  onChange={handleChange}
                  value={values.surname}
                  autoComplete="off"
                  placeholder="Smith"
                />
                <ErrorMessage name="surname">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isSurnameError,
                  })}
                />
              </div>
              <div className="form__input-wrapper">
                <label
                  className={classNames('form__label', {
                    form__label_error: isEmailError,
                  })}
                  htmlFor="email"
                >
                  Email
                </label>
                <Field
                  className={classNames('form__input', {
                    form__input_error: isEmailError,
                  })}
                  name="email"
                  type="email"
                  id="email"
                  disabled={isLoading}
                  onChange={handleChange}
                  value={values.email}
                  autoComplete="off"
                  placeholder="name@mail.com"
                />
                <ErrorMessage name="email">
                  {(msg: string) => <div className="form__error-text">{msg}</div>}
                </ErrorMessage>
                <ErrorSVG
                  className={classNames('form__error-icon', {
                    'form__error-icon_error': isEmailError,
                  })}
                />
              </div>
              <div className="form__input-wrapper">
                <label className="form__label" htmlFor="password">
                  Password
                </label>
                <Field
                  className="form__input"
                  name="password"
                  id="password"
                  disabled={true}
                  type="text"
                  onChange={handleChange}
                  autoComplete="new-password"
                  placeholder="password"
                />
              </div>
              <button
                type="submit"
                className="button button_size_large button_interaction"
                disabled={isLoading}
              >
                {isLoading ? 'Loading...' : 'Save changes'}
              </button>
            </form>
          );
        }}
      </Formik>
      <button
        type="button"
        className="modal__close-button button_interaction"
        onClick={() => setIsUpdateModalOpen(false)}
      >
        <CrossExitSVG className="modal__close-svg" />
      </button>
    </Modal>
  );
}
