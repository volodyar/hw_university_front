import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  setLectors,
  selectLectors,
  selectLectorsCount,
  useLazyFindAllLectorsQuery,
  Lector,
} from 'redux/lectors';
import Pagination from 'rc-pagination';
import { LectorsItem } from '../lectors-item/lectors-item';
import { PER_PAGE } from 'utils/constants';
import { useLocation } from 'react-router-dom';

interface ILectorsListProps {
  handleUpdate: (lector: Lector) => void;
}

const listTitles = ['Name', 'Surname', 'Email', 'Password'];

export function LectorsList({ handleUpdate }: ILectorsListProps) {
  const [currentPage, setCurrentPage] = useState(1);
  const { search } = useLocation();
  const [findAllLectors, { data, isSuccess }] = useLazyFindAllLectorsQuery();

  const dispatch = useDispatch();
  const lectors = useSelector(selectLectors);
  const count = useSelector(selectLectorsCount);

  useEffect(() => {
    setCurrentPage(1);
  }, [search]);

  useEffect(() => {
    if (search !== '') {
      findAllLectors({ search, limit: PER_PAGE, offset: currentPage });
    }
  }, [search, currentPage, findAllLectors]);

  useEffect(() => {
    if (isSuccess && data) {
      dispatch(setLectors({ lectors: data.lectors, count: data.count }));
    }
  }, [isSuccess, data, dispatch]);

  const handlePageClick = (page: number) => {
    setCurrentPage(page);
  };

  return (
    <>
      <ul className="title-list">
        {listTitles.map((title, index) => (
          <li key={index} className="title-list__item">
            {title}
          </li>
        ))}
      </ul>
      <ul className="entities-list">
        {lectors.map((lector) => (
          <LectorsItem key={lector.id} lector={lector} handleUpdate={handleUpdate} />
        ))}
      </ul>
      <Pagination
        total={count}
        pageSize={PER_PAGE}
        current={currentPage}
        onChange={handlePageClick}
        hideOnSinglePage={true}
        prevIcon={'<'}
        nextIcon={'>'}
      />
    </>
  );
}
