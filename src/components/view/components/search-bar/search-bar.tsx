import Select, { SingleValue } from 'react-select';
import { DebounceInput } from 'react-debounce-input';
import { CrossSVG, SearchLoopSVG } from 'svg';
import { useEffect, useState, Dispatch, SetStateAction } from 'react';
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom';
import './search-bar.styles.scss';
import { OptionValues, Pathname, SearchQuery } from 'utils/enums';

export interface IOption {
  label: string;
  value: string;
}

export const options: IOption[] = [
  { value: OptionValues.a_z, label: 'A-Z' },
  { value: OptionValues.z_a, label: 'Z-A' },
  { value: OptionValues.newest, label: 'Newest first' },
  { value: OptionValues.oldest, label: 'Oldest first' },
  { value: OptionValues.name, label: 'All' },
];

const studentsOptions: IOption[] = [
  ...options,
  { value: SearchQuery.course, label: 'Course' },
  { value: SearchQuery.group, label: 'Group' },
  { value: SearchQuery.surname, label: 'Surname' },
];

type SearchBarProps = {
  setIsCreateModalOpen: Dispatch<SetStateAction<boolean>>;
};

export function SearchBar({ setIsCreateModalOpen }: SearchBarProps) {
  const { pathname } = useLocation();
  const navigate = useNavigate();
  const [searchParam, setSearchParam] = useSearchParams();

  const [select, setSelect] = useState(options[4]);
  const [search, setSearch] = useState(() => searchParam.get(SearchQuery.name) ?? '');

  function onSearch({ target }: React.ChangeEvent<HTMLInputElement>) {
    const { value } = target;
    setSearch(value);
  }

  function onSelect(option: SingleValue<IOption>) {
    setSelect(option ?? options[0]);
  }

  useEffect(() => {
    studentsOptions.forEach((option) => searchParam.delete(option.value));

    let order = SearchQuery.ASC;
    let sort = SearchQuery.name;
    let searchBy = SearchQuery.name;
    let searchValue = search ?? '';

    if (select.value === SearchQuery.surname) searchBy = select.value;
    if (select.value === SearchQuery.group) searchBy = select.value;
    if (select.value === SearchQuery.course) searchBy = select.value;

    if (OptionValues.newest === select.value || OptionValues.oldest === select.value) {
      searchBy = SearchQuery.name;
      sort = SearchQuery.createdAt;
      if (select.value === OptionValues.newest) order = SearchQuery.DESC;
      if (select.value === OptionValues.oldest) order = SearchQuery.ASC;
    }

    if (OptionValues.a_z === select.value || OptionValues.z_a === select.value) {
      searchBy = SearchQuery.name;
      sort = SearchQuery.name;
      if (select.value === OptionValues.a_z) order = SearchQuery.ASC;
      if (select.value === OptionValues.z_a) order = SearchQuery.DESC;
    }

    searchParam.set(searchBy, searchValue);
    searchParam.set('sort', sort);
    searchParam.set('order', order);

    setSearchParam(searchParam);
  }, [select, search, searchParam, setSearchParam]);

  return (
    <div className="search-bar">
      <form className="search-bar__form">
        <p className="search-bar__label">Sort by</p>
        <Select
          classNamePrefix="react-select"
          className="react-select"
          value={select}
          onChange={onSelect}
          options={pathname === Pathname.students ? studentsOptions : options}
        />
        <div className="search-bar__input-wrapper">
          <DebounceInput
            className="search-bar__input"
            onChange={onSearch}
            debounceTimeout={500}
            type="text"
            name="search"
            value={search}
            placeholder="Search"
          />
          <SearchLoopSVG className="search-bar__input-svg" />
        </div>
      </form>
      <button
        type="button"
        onClick={() => {
          if (pathname !== Pathname.students) {
            setIsCreateModalOpen(true);
          } else {
            navigate(`${Pathname.students}/create`);
          }
        }}
        className="search-bar__button button_interaction"
      >
        <CrossSVG className="search-bar__button-svg" />
        <span className="search-bar__button-text">{`Add new ${pathname.substring(1)}`}</span>
      </button>
    </div>
  );
}
