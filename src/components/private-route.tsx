import { Navigate, Outlet, useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { selectIsLoggedIn } from 'redux/auth/auth.slice';

interface PrivateRouteProps {
  redirectTo: string;
}

export function PrivateRoute({ redirectTo }: PrivateRouteProps) {
  const location = useLocation();
  const isLoggedIn = useSelector(selectIsLoggedIn);
  const shouldRedirect = !isLoggedIn;

  return shouldRedirect ? (
    <Navigate to={redirectTo} state={{ from: location }} replace />
  ) : (
    <Outlet />
  );
}
