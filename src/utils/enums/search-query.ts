export enum SearchQuery {
  name = 'name',
  group = 'group',
  course = 'course',
  surname = 'surname',
  createdAt = 'createdAt',
  ASC = 'ASC',
  DESC = 'DESC',
}
