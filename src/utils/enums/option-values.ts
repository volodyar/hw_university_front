export enum OptionValues {
  newest = 'newest',
  oldest = 'oldest',
  name = 'name',
  a_z = 'a-z',
  z_a = 'z-a',
}
