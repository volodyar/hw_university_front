export enum Pathname {
  lectors = '/lectors',
  students = '/students',
  groups = '/groups',
  courses = '/courses',
}
