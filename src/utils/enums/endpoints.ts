export enum Endpoints {
  lectors = 'lectors',
  students = 'students',
  groups = 'groups',
  courses = 'courses',
}
