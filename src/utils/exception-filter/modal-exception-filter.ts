import { SerializedError } from '@reduxjs/toolkit';
import { FetchBaseQueryError } from '@reduxjs/toolkit/dist/query';

export function modalExceptionFilter(error: FetchBaseQueryError | SerializedError | undefined) {
  let message = 'Something went wrong!';

  if ('status' in error!) {
    const { status } = error;
    if (status === 400 || status === 403) {
      const { data } = error as { data: { message: string } };
      if (typeof data.message === 'string') message = data.message;
      if (Array.isArray(data.message)) message = data.message[0];
    }
  }

  return message;
}
