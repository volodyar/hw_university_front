import { SerializedError } from '@reduxjs/toolkit';
import { FetchBaseQueryError } from '@reduxjs/toolkit/dist/query';

export function loginExceptionFilter(error: FetchBaseQueryError | SerializedError | undefined) {
  let message = 'Something went wrong!';

  if ('status' in error!) {
    const { status } = error;
    if (status === 403) {
      message = "We can't find that email and password combination!";
    }

    if (status === 400) {
      message = 'Invalid email or password!';
    }
  }

  return message;
}
