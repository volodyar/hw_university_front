import { SerializedError } from '@reduxjs/toolkit';
import { FetchBaseQueryError } from '@reduxjs/toolkit/dist/query';

export function signupExceptionFilter(error: FetchBaseQueryError | SerializedError | undefined) {
  let message = 'Something went wrong!';

  if ('status' in error!) {
    const { status } = error;
    if (status === 403) {
      message = 'Email in use!';
    }
    if (status === 400) {
      message = 'Invalid email or password!';
    }
  }

  return message;
}
