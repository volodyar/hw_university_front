import { SerializedError } from '@reduxjs/toolkit';
import { FetchBaseQueryError } from '@reduxjs/toolkit/dist/query';

export function resetPasswordExceptionFilter(
  error: FetchBaseQueryError | SerializedError | undefined,
) {
  let message = 'Something went wrong!';

  if ('status' in error!) {
    const { status } = error;
    if (status === 400) {
      message = 'Invalid password';
    }

    if (status === 403) {
      message = 'User not found';
    }
  }

  return message;
}
