import { SerializedError } from '@reduxjs/toolkit';
import { FetchBaseQueryError } from '@reduxjs/toolkit/dist/query';

export function forgetPasswordExceptionFilter(
  error: FetchBaseQueryError | SerializedError | undefined,
) {
  let message = 'Something went wrong!';

  if ('status' in error!) {
    const { status } = error;
    if (status === 400) {
      message = "We can't find that email!";
    }
  }

  return message;
}
