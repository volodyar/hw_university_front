import { apiSlice } from 'redux/api/api-slice';
import { Group, IGroupsState } from './groups.slice';
import { IGroupsAdd } from 'components/view/components/groups/create-group-modal/create-group-modal';
import { IGroupUpdate } from 'components/view/components/groups/update-group-modal/update-group-modal';
import { Endpoints, Pathname } from 'utils/enums';

const groupsApiSlice = apiSlice
  .enhanceEndpoints({ addTagTypes: [Endpoints.groups] })
  .injectEndpoints({
    endpoints: (builder) => ({
      findAllGroups: builder.query<IGroupsState, { search: string; limit: number; offset: number }>(
        {
          query: ({ search, limit, offset }) => {
            return {
              url: `${Pathname.groups}${search}&limit=${limit}&offset=${offset}`,
              method: 'GET',
            };
          },
          providesTags: (result, error, arg) => {
            const groups = result?.groups!;
            return groups
              ? [
                  ...groups.map(({ id }) => ({ type: Endpoints.groups as const, id })),
                  Endpoints.groups,
                ]
              : [Endpoints.groups];
          },
        },
      ),
      createGroup: builder.mutation<Group, IGroupsAdd>({
        query: (values) => ({ url: Pathname.groups, method: 'POST', body: values }),
        invalidatesTags: [Endpoints.groups],
      }),
      updateGroup: builder.mutation<Group, { body: IGroupUpdate; id: number | null }>({
        query: (values) => ({
          url: `${Pathname.groups}/${values.id}`,
          method: 'PATCH',
          body: values.body,
        }),
        invalidatesTags: (result, error, arg) => [{ type: Endpoints.groups, id: arg.id! }],
      }),
    }),
  });

export const {
  useFindAllGroupsQuery,
  useCreateGroupMutation,
  useUpdateGroupMutation,
  useLazyFindAllGroupsQuery,
} = groupsApiSlice;
