import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from 'redux/store';
import { Student } from '../students/students.slice';
import { Endpoints } from 'utils/enums';

export type Group = {
  id: number;
  name: string;
  students: Student[];
};

export interface IGroupsState {
  groups: Group[] | [];
  count: number;
}

const initialState: IGroupsState = {
  groups: [],
  count: 0,
};

const groupsSlice = createSlice({
  name: Endpoints.groups,
  initialState,
  reducers: {
    setGroups: (state, { payload }: PayloadAction<IGroupsState>) => {
      state.groups = payload.groups;
      state.count = payload.count;
    },
  },
});

export const { setGroups } = groupsSlice.actions;
export const selectGroups = (state: RootState) => state.groups.groups;
export const selectGroupsCount = (state: RootState) => state.groups.count;
export const groupsReducer = groupsSlice.reducer;
