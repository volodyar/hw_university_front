import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import type { RootState } from 'redux/store';

export interface IAuthState {
  user: {
    email: string;
  } | null;
  token: string | null;
  isLoggedIn: boolean;
}

const initialState: IAuthState = {
  user: null,
  token: null,
  isLoggedIn: false,
};

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setCredentials: (state, { payload }: PayloadAction<Omit<IAuthState, 'isLoggedIn'>>) => {
      state.user = payload.user;
      state.token = payload.token;
      state.isLoggedIn = true;
    },
    logout: () => initialState,
  },
});

const persistConfig = {
  key: 'auth',
  storage,
  blacklist: ['token'],
};

export const { setCredentials, logout } = authSlice.actions;
export const selectUser = (state: RootState) => state.auth.user;
export const selectToken = (state: RootState) => state.auth.token;
export const selectIsLoggedIn = (state: RootState) => state.auth.isLoggedIn;
export const persistedAuth = persistReducer(persistConfig, authSlice.reducer);
