import { apiSlice } from 'redux/api/api-slice';
import { IAuthState } from './auth.slice';
import { ILoginRequest } from 'components/view/components/login-form/login-form';
import { IRegisterRequest } from 'components/view/components/register-form/register-form';
import { IForgetPasswordRequest } from 'components/view/components/forget-password-form/forget-password-form';
import { IResetPasswordRequest } from 'components/view/components/reset-password-form/reset-passwor-form';

export interface LoginResponse {
  email: string;
  access_token: string;
}

export const authApiSlice = apiSlice.enhanceEndpoints({ addTagTypes: ['auth'] }).injectEndpoints({
  endpoints: (builder) => ({
    login: builder.mutation<any, ILoginRequest>({
      query: (loginProp) => ({
        url: '/auth/signin',
        method: 'POST',
        body: loginProp,
      }),
      transformResponse: ({
        email,
        access_token,
      }: LoginResponse): Omit<IAuthState, 'isLoggedIn'> => {
        return {
          user: { email },
          token: access_token,
        };
      },
      invalidatesTags: ['auth'],
    }),
    register: builder.mutation<void, IRegisterRequest>({
      query: (registerProps) => ({
        url: '/auth/signup',
        method: 'POST',
        body: registerProps,
      }),
    }),
    logout: builder.mutation<void, void>({
      query: () => ({
        url: '/auth/logout',
        method: 'POST',
      }),
      invalidatesTags: ['auth'],
    }),
    resetPasswordRequest: builder.mutation<void, IForgetPasswordRequest>({
      query: (forgetPasswordProps) => ({
        url: '/auth/reset-password-request',
        method: 'POST',
        body: forgetPasswordProps,
      }),
    }),
    resetPassword: builder.mutation<void, IResetPasswordRequest>({
      query: (resetPasswordProps) => ({
        url: '/auth/reset-password',
        method: 'POST',
        body: resetPasswordProps,
      }),
    }),
  }),
});

export const {
  useLoginMutation,
  useLogoutMutation,
  useRegisterMutation,
  useResetPasswordRequestMutation,
  useResetPasswordMutation,
} = authApiSlice;
