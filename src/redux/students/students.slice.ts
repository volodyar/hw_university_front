import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from 'redux/store';
import { Course } from '../courses';
import { Endpoints } from 'utils/enums';

export type Student = {
  id: number;
  name: string;
  surname: string;
  email: string;
  age: number;
  courses: Course[];
  group: { name: string } | null;
  groupId: number;
  imagePath: string | null;
};

export interface IStudentsState {
  students: Student[] | [];
  count: number;
}

const initialState: IStudentsState = {
  students: [],
  count: 0,
};

const studentsSlice = createSlice({
  name: Endpoints.students,
  initialState,
  reducers: {
    setStudents: (state, { payload }: PayloadAction<IStudentsState>) => {
      state.students = payload.students;
      state.count = payload.count;
    },
  },
});

export const { setStudents } = studentsSlice.actions;
export const selectStudents = (state: RootState) => state.students.students;
export const selectStudentsCount = (state: RootState) => state.students.count;
export const studentsReducer = studentsSlice.reducer;
