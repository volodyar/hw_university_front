import { apiSlice } from 'redux/api/api-slice';
import { IStudentsState, Student } from './students.slice';
import { IStudentAdd } from 'components/view/components/students/create-student/create-student-form';
import { FetchBaseQueryError } from '@reduxjs/toolkit/dist/query';
import { IStudentUpdate } from 'components/view/components/students/update-student/update-student-form';
import { Endpoints, Pathname } from 'utils/enums';

interface IStudentApiCreate extends Omit<IStudentAdd, 'groupId' | 'courseId' | 'photo'> {
  photo: File;
}

interface IStudentApiUpdate extends Omit<IStudentUpdate, 'photo'> {
  photo: File | undefined;
}

const studentsApiSlice = apiSlice
  .enhanceEndpoints({ addTagTypes: [Endpoints.students] })
  .injectEndpoints({
    endpoints: (builder) => ({
      findAllStudents: builder.query<
        IStudentsState,
        { search: string; limit: number; offset: number }
      >({
        query: ({ search, limit, offset }) => ({
          url: `${Pathname.students}${search}&limit=${limit}&offset=${offset}`,
          method: 'GET',
        }),
        providesTags: [Endpoints.students],
      }),
      findOneStudent: builder.query<Student, { id: string }>({
        query: ({ id }) => ({
          url: `${Pathname.students}/${id}`,
          method: 'GET',
        }),
        providesTags: [Endpoints.students],
      }),
      createStudent: builder.mutation<Student, IStudentApiCreate>({
        async queryFn(arg, api, extraOptions, baseQuery) {
          const { name, course, surname, group, age, email, photo } = arg;
          const { value: groupId } = group;
          const { value: courseId } = course;

          if (groupId === 0) {
            const error = {
              status: 400,
              data: { message: 'The group is not set' },
            } as FetchBaseQueryError;
            return { error };
          }

          const createResult = await baseQuery({
            url: Pathname.students,
            method: 'POST',
            body: { name, surname, groupId, age, email },
          });

          let isSuccess = createResult.meta?.response?.ok;
          let student = createResult.data as Student;
          let error = createResult.error as FetchBaseQueryError;

          if (photo) {
            if (createResult.error) return { error };

            const formData = new FormData();
            formData.append('file', photo);

            const fileResult = await baseQuery({
              url: `${Pathname.students}/upload-photo/${student.id}`,
              method: 'PATCH',
              body: formData,
            });

            isSuccess = fileResult.meta?.response?.ok;
            error = fileResult.error as FetchBaseQueryError;
          }

          if (courseId !== 0) {
            if (createResult.error) return { error };

            const addResult = await baseQuery({
              url: `${Pathname.students}/add-course`,
              method: 'POST',
              body: { studentId: student.id, courseId },
            });

            isSuccess = addResult.meta?.response?.ok;
            error = addResult.error as FetchBaseQueryError;
          }

          return isSuccess ? { data: student } : { error };
        },
        invalidatesTags: [Endpoints.students],
      }),
      updateStudent: builder.mutation<
        Student,
        {
          body: IStudentApiUpdate;
          id: string;
        }
      >({
        async queryFn(arg, api, extraOptions, baseQuery) {
          const { name, surname, group, age, email, photo, course } = arg.body;
          const { value: groupId } = group;
          const { value: courseId } = course;

          const updateResult = await baseQuery({
            url: `${Pathname.students}/${arg.id}`,
            method: 'PATCH',
            body: { name, surname, groupId, age, email },
          });

          let isSuccess = updateResult.meta?.response?.ok;
          let student = updateResult.data as Student;
          let error = updateResult.error as FetchBaseQueryError;

          if (photo) {
            if (updateResult.error) return { error };

            const formData = new FormData();
            formData.append('file', photo);

            const fileResult = await baseQuery({
              url: `${Pathname.students}/upload-photo/${student.id}`,
              method: 'PATCH',
              body: formData,
            });

            isSuccess = fileResult.meta?.response?.ok;
            error = fileResult.error as FetchBaseQueryError;
          }

          if (courseId !== 0) {
            if (updateResult.error) return { error };

            const addResult = await baseQuery({
              url: `${Pathname.students}/add-course`,
              method: 'POST',
              body: { studentId: student.id, courseId },
            });

            isSuccess = addResult.meta?.response?.ok;
            error = addResult.error as FetchBaseQueryError;
          }

          return isSuccess ? { data: student } : { error };
        },
        invalidatesTags: [Endpoints.students],
      }),
    }),
  });

export const {
  useLazyFindAllStudentsQuery,
  useCreateStudentMutation,
  useUpdateStudentMutation,
  useLazyFindOneStudentQuery,
} = studentsApiSlice;
