import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from 'redux/store';
import { Endpoints } from 'utils/enums';

export type Course = {
  id: number;
  name: string;
  description: string;
  hours: number;
  studentsCount: number;
};

export interface ICoursesState {
  courses: Course[] | [];
  count: number;
}

const initialState: ICoursesState = {
  courses: [],
  count: 0,
};

const coursesSlice = createSlice({
  name: Endpoints.courses,
  initialState,
  reducers: {
    setCourses: (state, { payload }: PayloadAction<ICoursesState>) => {
      state.courses = payload.courses;
      state.count = payload.count;
    },
  },
});

export const { setCourses } = coursesSlice.actions;
export const selectCourses = (state: RootState) => state.courses.courses;
export const selectCoursesCount = (state: RootState) => state.courses.count;
export const coursesReducer = coursesSlice.reducer;
