import { apiSlice } from 'redux/api/api-slice';
import { Course, ICoursesState } from './courses.slice';
import { ICourseAdd } from 'components/view/components/courses/create-course-modal/create-course-modal';
import { ICourseUpdate } from 'components/view/components/courses/update-course-modal/update-course-modal';
import { Endpoints, Pathname } from 'utils/enums';

const coursesApiSlice = apiSlice
  .enhanceEndpoints({ addTagTypes: [Endpoints.courses] })
  .injectEndpoints({
    endpoints: (builder) => ({
      findAllCourses: builder.query<
        ICoursesState,
        { search: string; limit: number; offset: number }
      >({
        query: ({ search, limit, offset }) => ({
          url: `${Pathname.courses}${search}&limit=${limit}&offset=${offset}`,
          method: 'GET',
        }),
        providesTags: (result, error, arg) => {
          const courses = result?.courses!;
          return courses
            ? [
                ...courses.map(({ id }) => ({ type: Endpoints.courses as const, id })),
                Endpoints.courses,
              ]
            : [Endpoints.courses];
        },
      }),
      createCourse: builder.mutation<Course, ICourseAdd>({
        query: (values) => ({ url: Pathname.courses, method: 'POST', body: values }),
        invalidatesTags: [Endpoints.courses],
      }),
      updateCourse: builder.mutation<Course, { body: ICourseUpdate; id: number | null }>({
        query: (values) => ({
          url: `${Pathname.courses}/${values.id}`,
          method: 'PATCH',
          body: values.body,
        }),
        invalidatesTags: (result, error, arg) => [{ type: Endpoints.courses, id: arg.id! }],
      }),
    }),
  });

export const {
  useFindAllCoursesQuery,
  useCreateCourseMutation,
  useUpdateCourseMutation,
  useLazyFindAllCoursesQuery,
} = coursesApiSlice;
