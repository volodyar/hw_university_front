import { BaseQueryApi, FetchArgs, createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { IAuthState, logout, setCredentials } from 'redux/auth/auth.slice';
import { LoginResponse } from '../auth/auth-api.slice';
import { SERVER_HOST } from 'utils/constants';

type state = {
  auth: IAuthState;
};

const baseQuery = fetchBaseQuery({
  baseUrl: `${SERVER_HOST}/api/v1`,
  credentials: 'include',
  prepareHeaders: (headers, { getState }) => {
    const { auth } = getState() as state;
    if (auth.token) {
      headers.set('authorization', `Bearer ${auth.token}`);
    }

    return headers;
  },
});

async function baseQueryWithReauth(
  args: string | FetchArgs,
  api: BaseQueryApi,
  extraOptions: { [key: string]: unknown },
) {
  let result = await baseQuery(args, api, extraOptions);

  if (result?.error?.status === 401) {
    const refreshResult = await baseQuery(
      { url: '/auth/refresh-token', method: 'GET' },
      api,
      extraOptions,
    );

    if (refreshResult?.data) {
      const { access_token } = refreshResult.data as LoginResponse;
      const { auth } = api.getState() as state;

      api.dispatch(
        setCredentials({
          user: auth.user,
          token: access_token,
        }),
      );

      result = await baseQuery(args, api, extraOptions);
    } else {
      api.dispatch(logout());
    }
  }

  return result;
}

export const apiSlice = createApi({
  baseQuery: baseQueryWithReauth,
  endpoints: () => ({}),
});
