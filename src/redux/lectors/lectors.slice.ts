import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from 'redux/store';
import { Endpoints } from 'utils/enums';

export type Lector = {
  id: number;
  name: string;
  surname: string;
  email: string;
  password: string;
};

export interface ILectorsState {
  lectors: Lector[] | [];
  count: number;
}

const initialState: ILectorsState = {
  lectors: [],
  count: 1,
};

const lectorsSlice = createSlice({
  name: Endpoints.lectors,
  initialState,
  reducers: {
    setLectors: (state, { payload }: PayloadAction<ILectorsState>) => {
      state.lectors = payload.lectors;
      state.count = payload.count;
    },
  },
});

export const { setLectors } = lectorsSlice.actions;
export const selectLectors = (state: RootState) => state.lectors.lectors;
export const selectLectorsCount = (state: RootState) => state.lectors.count;
export const lectorsReducer = lectorsSlice.reducer;
