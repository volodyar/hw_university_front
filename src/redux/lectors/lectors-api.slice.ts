import { apiSlice } from 'redux/api/api-slice';
import { ILectorsState, Lector } from './lectors.slice';
import { ILectorAdd } from 'components/view/components/lectors/create-lector-modal/create-lector-modal';
import { ILectorUpdate } from 'components/view/components/lectors/update-lector-modal/update-lector-modal';
import { Endpoints, Pathname } from 'utils/enums';

const lectorsApiSlice = apiSlice
  .enhanceEndpoints({ addTagTypes: [Endpoints.lectors] })
  .injectEndpoints({
    endpoints: (builder) => ({
      findAllLectors: builder.query<
        ILectorsState,
        { search: string; limit: number; offset: number }
      >({
        query: ({ search, limit, offset }) => ({
          url: `${Pathname.lectors}${search}&limit=${limit}&offset=${offset}`,
          method: 'GET',
        }),
        providesTags: (result, error, arg) => {
          const lectors = result?.lectors!;
          return lectors
            ? [
                ...lectors.map(({ id }) => ({ type: Endpoints.lectors as const, id })),
                Endpoints.lectors,
              ]
            : [Endpoints.lectors];
        },
      }),
      createLector: builder.mutation<Lector, ILectorAdd>({
        query: (values) => ({ url: Pathname.lectors, method: 'POST', body: values }),
        invalidatesTags: [Endpoints.lectors],
      }),
      updateLector: builder.mutation<Lector, { body: ILectorUpdate; id: number | null }>({
        query: (values) => ({
          url: `${Pathname.lectors}/${values.id}`,
          method: 'PATCH',
          body: values.body,
        }),
        invalidatesTags: (result, error, arg) => [{ type: Endpoints.lectors, id: arg.id! }],
      }),
    }),
  });

export const {
  useLazyFindAllLectorsQuery,
  useFindAllLectorsQuery,
  useCreateLectorMutation,
  useUpdateLectorMutation,
} = lectorsApiSlice;
