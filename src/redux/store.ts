import { configureStore } from '@reduxjs/toolkit';
import { persistStore, FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER } from 'redux-persist';
import { apiSlice } from './api/api-slice';
import { persistedAuth } from './auth';
import { lectorsReducer } from './lectors';
import { studentsReducer } from './students';
import { coursesReducer } from './courses';
import { groupsReducer } from './groups';

export const store = configureStore({
  reducer: {
    auth: persistedAuth,
    lectors: lectorsReducer,
    students: studentsReducer,
    courses: coursesReducer,
    groups: groupsReducer,
    [apiSlice.reducerPath]: apiSlice.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }).concat(apiSlice.middleware),
});

export const persistor = persistStore(store);
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
